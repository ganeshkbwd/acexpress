package hnweb.com.acexpress.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;
import hnweb.com.acexpress.utility.Validations;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    EditText oldPassET, newPassET, confirmPassET;
    String oldPass, newPass, confirmPass;
    private LoadingDialog loadingDialog;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        loadingDialog = new LoadingDialog(this);
        setToolbarDrawer();
        init();

    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(" Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
////                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();

    }

    public void init() {
        oldPassET = (EditText) findViewById(R.id.oldPassET);
        newPassET = (EditText) findViewById(R.id.newPassET);
        confirmPassET = (EditText) findViewById(R.id.confirmPassET);
    }

    public void getData() {
        oldPass = oldPassET.getText().toString().trim();
        newPass = newPassET.getText().toString().trim();
        confirmPass = confirmPassET.getText().toString().trim();
    }

    public void checkValid() {
        getData();
        if (Validations.strslength(oldPass) && Validations.strslength(newPass) && Validations.confirmPass(newPass, confirmPass)) {
//            ToastUlility.show(this, "Coming soon ...");
            doChangePassword(oldPass, newPass);
        } else if (Validations.strslength(oldPass) == false && Validations.strslength(newPass) == false && Validations.confirmPass(newPass, confirmPass) == false) {
            oldPassET.setError("Please enter old password.");
            oldPassET.requestFocus();
            newPassET.setError("Please enter new password.");
            confirmPassET.setError("Please enter confirm password.");
        } else if (Validations.strslength(oldPass) == false) {
            oldPassET.setError("Please enter old password.");
            oldPassET.requestFocus();
        } else if (Validations.strslength(newPass) == false) {
            newPassET.setError("Please enter new password.");
            newPassET.requestFocus();
        } else if (Validations.confirmPass(newPass, confirmPass) == false) {
            confirmPassET.setError("New Password and confirm password not same.");
            confirmPassET.requestFocus();
        } else {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.savePassBTN:
                checkValid();

                break;
        }
    }
//    public void onDrawerClick(View v) {
//
//        DrawerClick.drawerClick(this, v.getId());
//    }

    public void doChangePassword(final String oldPass, final String newPass) {
        loadingDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.CHANGEPASS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int message_code = jsonObject.getInt("message_code");
                    if (message_code == 1) {
                        oldPassET.setText("");
                        newPassET.setText("");
                        confirmPassET.setText("");
                    } else {

                    }
                    String message = jsonObject.getString("message");

                    ToastUlility.show(ChangePasswordActivity.this, message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.USER_ID, String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
                params.put(AppParams.OLDPASS, oldPass);
                params.put(AppParams.NEWPASS, newPass);
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        String request_tag = "change_password";

        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(this, MyProfileActivity.class);
        startActivity(intent);
        finish();
    }
}
