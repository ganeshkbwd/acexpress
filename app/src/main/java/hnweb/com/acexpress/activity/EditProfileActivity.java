package hnweb.com.acexpress.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.PickerUtils;
import hnweb.com.acexpress.utility.ScalingUtilities;
import hnweb.com.acexpress.utility.ToastUlility;
import hnweb.com.acexpress.utility.Validations;


public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    private static final int FROM_GALLARY = 103;
    private static final int REQUEST_CAMERA = 5;
    String encodedImage1;
    ImageView proPickIV;
    private static final int REQUEST_IMAGE = 2;
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    protected static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
    private ArrayList<String> mSymSelectPath = new ArrayList<String>();
    EditText nameET, emailET, phoneET;
    String name, email, phone;
    SharedPreferences sharedPreferences;
    private LoadingDialog loadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        loadingDialog = new LoadingDialog(this);
        setToolbarDrawer();
        init();
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(" Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    public void init() {
        proPickIV = (ImageView) findViewById(R.id.proPickIV);
        nameET = (EditText) findViewById(R.id.nameET);
        emailET = (EditText) findViewById(R.id.emailET);
        phoneET = (EditText) findViewById(R.id.phoneET);

        if (!sharedPreferences.getString("PROFILE_PHOTO", "").equalsIgnoreCase("")) {
            Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", "")).override(150, 150).into(proPickIV);
        }
        nameET.setText(sharedPreferences.getString("NAME", null));
        emailET.setText(sharedPreferences.getString("EMAIL_ID", null));
        phoneET.setText(sharedPreferences.getString("PHONE", null));
    }

    public void getData() {
        name = nameET.getText().toString().trim();
        email = emailET.getText().toString().trim();
        phone = phoneET.getText().toString().trim();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cameraBTN:
                PickerUtils.selectImageDialog(this, REQUEST_CAMERA, FROM_GALLARY);
//                pickImage();
                break;
            case R.id.saveBTN:
                checkValidation();

                break;
        }
    }

    public void checkValidation() {

        getData();
        if (Validations.strslength(name) && Validations.emailCheck(email) && Validations.strslength(phone)) {
//            ToastUlility.show(this, "Coming soon ...");
            doEditProfile();
        } else if (Validations.strslength(name) == false && Validations.emailCheck(email) == false && Validations.strslength(phone) == false) {

            nameET.setError("Please enter name.");
            nameET.requestFocus();
            emailET.setError("Please enter valid email");
            phoneET.setError("Please enter phone number.");
        } else if (Validations.strslength(name) == false) {
            nameET.setError("Please enter name.");
            nameET.requestFocus();

        } else if (Validations.strslength(email) == false) {
            emailET.setError("Please enter valid email");
            emailET.requestFocus();

        } else if (Validations.strslength(phone) == false) {
            phoneET.setError("Please enter phone number.");
            phoneET.requestFocus();
        }else {

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FROM_GALLARY) {

                System.out.println("SELECT_IMAGE");
                onSelectFromGalleryResult(data);
//                Uri uri = data.getData();
//
//                encodedImage1 = PickerUtils.gallaryPath(EditProfileActivity.this, uri);
//
//                Glide.with(this).load(new File(encodedImage1)).transform(new RoundImageTransform(EditProfileActivity.this)).into(proPickIV);
//
//                encodedImage1 = PickerUtils.encodeImage(encodedImage1);


            } else if (requestCode == REQUEST_CAMERA) {

                onCaptureImageResult(data);

//                System.out.println("REQUEST_CAMERA");
//
//                encodedImage1 = PickerUtils.cameraPath();
//
//                Glide.with(this).load(new File(encodedImage1)).transform(new RoundImageTransform(EditProfileActivity.this)).into(proPickIV);
//                encodedImage1 = PickerUtils.encodeImage(encodedImage1);
            }

        }
    }


    public void doEditProfile() {
        loadingDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.EDITPROFILE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int message_code = jsonObject.getInt("message_code");
                    String message = jsonObject.getString("message");

                    ToastUlility.show(EditProfileActivity.this, message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.USER_ID, String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
                params.put(AppParams.ADDRESS, email);
                params.put(AppParams.PHONE, phone);
                params.put(AppParams.NAME, name);
                params.put(AppParams.PROFILE_PHOTO, encodedImage1);
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        String request_tag = "change_password";

        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,MyProfileActivity.class);
        startActivity(intent);
        finish();
//        super.onBackPressed();
    }


//    private void onSelectFromGalleryResult(Intent data) {
//        Uri selectedImageUri = data.getData();
//        String[] projection = {MediaStore.MediaColumns.DATA};
//        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
//                null);
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        cursor.moveToFirst();
//
//        String selectedImagePath = cursor.getString(column_index);
//        BitmapFactory.Options options = new BitmapFactory.Options();
//
//        Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);
//
//        Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bm, 150, 150, ScalingUtilities.ScalingLogic.CROP);
//
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        byte[] byteArrayImage = stream.toByteArray();
//        encodedImage1 = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
//
////        profileImage = scaledBitmap;
//        proPickIV.setImageBitmap(scaledBitmap);
////        IMAGESET = 1;
//
//    }
//
//
//    private void onCaptureImageResult(Intent data) {
//        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//
//        int maxHeight = 150;
//        int maxWidth = 150;
//        float scale = Math.min(((float) maxHeight / thumbnail.getWidth()), ((float) maxWidth / thumbnail.getHeight()));
//
//        Matrix matrix = new Matrix();
//        matrix.postScale(scale, scale);
//        Bitmap scaledBitmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
//
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        thumbnail.compress(Bitmap.CompressFormat.PNG, 70, stream);
//        byte[] byteArrayImage = stream.toByteArray();
//        encodedImage1 = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
//
////        profileImage = scaledBitmap;
//        proPickIV.setImageBitmap(scaledBitmap);
////        IMAGESET = 1;
////        imageUpload();
//
//    }


    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);
        BitmapFactory.Options options = new BitmapFactory.Options();

        Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);

        Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bm, 150, 150, ScalingUtilities.ScalingLogic.CROP);
//============================================================================================================
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
// get the base 64 string
        encodedImage1 = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
//============================================================================================================
// ByteArrayOutputStream stream = new ByteArrayOutputStream();
// scaledBitmap.compress(Bitmap.CompressFormat.PNG, 60, stream);
// byte[] byteArrayImage = stream.toByteArray();
// encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
// Log.e("encodedImage Gallery",encodedImage);


// profileImage = scaledBitmap;
        proPickIV.setImageBitmap(scaledBitmap);
//IMAGESET = 1;

    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        int maxHeight = 150;
        int maxWidth = 150;
        float scale = Math.min(((float) maxHeight / thumbnail.getWidth()), ((float) maxWidth / thumbnail.getHeight()));

        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap scaledBitmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 70, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage1 = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
        Log.e("encodedImage camera",encodedImage1);
// profileImage = scaledBitmap;
        proPickIV.setImageBitmap(scaledBitmap);
//IMAGESET = 1;
// imageUpload();
    }
}
