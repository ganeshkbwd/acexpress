package hnweb.com.acexpress.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;
import hnweb.com.acexpress.utility.Validations;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edit_email, edit_new_pwd, edit_otp, edit_con_pwd;
    private LoadingDialog loadingDialog;
    Button back_to_loginBTN;
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        edit_email = (EditText) findViewById(R.id.edit_email);
        edit_otp = (EditText) findViewById(R.id.edit_otp);
        edit_new_pwd = (EditText) findViewById(R.id.edit_new_pwd);
        edit_con_pwd = (EditText) findViewById(R.id.edit_con_pwd);
        back_to_loginBTN = (Button) findViewById(R.id.back_to_loginBTN);
        loadingDialog = new LoadingDialog(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signup_imageView:
                if (Validations.emailCheck(edit_email.getText().toString().trim())) {

                    doForgotPassword(edit_email.getText().toString().trim());
//                    Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
                } else {
                    edit_email.setError("Please enter valid email address.");
                }

                break;
            case R.id.signup_imageView1:
                if (Validations.strslength(edit_otp.getText().toString().trim())) {
                    if (Validations.strslength(edit_new_pwd.getText().toString().trim())) {
                        if (Validations.confirmPass(edit_new_pwd.getText().toString().trim(), edit_con_pwd.getText().toString().trim())) {
//                            Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
                            doOTP(edit_otp.getText().toString().trim(), edit_new_pwd.getText().toString().trim());
                        } else {
                            edit_con_pwd.setError("Password and confirm password not match.");
                        }
                    } else {
                        edit_new_pwd.setError("Please enter new password.");
                    }
                } else {
                    edit_otp.setError("Please enter OTP.");
                }
                break;
            case R.id.back_to_loginBTN:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public void doForgotPassword(final String email) {
        loadingDialog.show();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.FORGOT_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jobj = new JSONObject(response);
                    int message_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    ToastUlility.show(ForgotPasswordActivity.this, message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.EMAIL_ID, email);
//                params.put(AppParams.PASSWORD, password);
//                params.put(AppParams.USER_TYPE, type);
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        String request_tag = "forgot_password";
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void doOTP(final String otp, final String password) {
        loadingDialog.show();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.OTP_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jobj = new JSONObject(response);
                    int message_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    ToastUlility.show(ForgotPasswordActivity.this, message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.OTP, otp);
                params.put(AppParams.NEWPASS, password);
//                params.put(AppParams.USER_TYPE, type);
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        String request_tag = "forgot_password";
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }
}
