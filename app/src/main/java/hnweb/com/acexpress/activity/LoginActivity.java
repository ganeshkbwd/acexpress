package hnweb.com.acexpress.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.customer.FindDriversActivity;
import hnweb.com.acexpress.activity.driver.HomeActivity;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.utility.DrawerClick;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;
import hnweb.com.acexpress.utility.TrackGPS;
import hnweb.com.acexpress.utility.Validations;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText emailET, passwordET;
    TextView forgetpassword_imageView;
    private LoadingDialog loadingDialog;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private TrackGPS gps;
    double longitude;
    double latitude;
    CheckBox remember_checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        editor = prefs.edit();

        init();


    }

    public void init() {
        String fp = "<font color=" + "#ffffff" + ">" + "Forgot your Password?" + "</font>";
        String ch = "<font color=" + "#ff3838" + ">" + "Click here" + "</font>";

        forgetpassword_imageView = (TextView) findViewById(R.id.forgetpassword_imageView);
        forgetpassword_imageView.setText(Html.fromHtml(fp + ch));
        emailET = (EditText) findViewById(R.id.emailET);
        passwordET = (EditText) findViewById(R.id.passwordET);

        remember_checkBox = (CheckBox) findViewById(R.id.remember_checkBox);


        if (AppConstants.ISDRIVER) {
            SharedPreferences rem_prefs_d = getSharedPreferences(getPackageName() + "REM_D", Context.MODE_PRIVATE);
            String rem_email_d = rem_prefs_d.getString("EMAIL", "");
            String rem_pass_d = rem_prefs_d.getString("PASS", "");
            if (!TextUtils.isEmpty(rem_email_d)) {
                emailET.setText(rem_email_d);
                passwordET.setText(rem_pass_d);
                remember_checkBox.setChecked(true);
            }


        } else {
            SharedPreferences rem_prefs_c = getSharedPreferences(getPackageName() + "REM_C", Context.MODE_PRIVATE);
            String rem_email_c = rem_prefs_c.getString("EMAIL", "");
            String rem_pass_c = rem_prefs_c.getString("PASS", "");
            if (!TextUtils.isEmpty(rem_email_c)) {
                emailET.setText(rem_email_c);
                passwordET.setText(rem_pass_c);
                remember_checkBox.setChecked(true);
            }

        }


        remember_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    if (!TextUtils.isEmpty(emailET.getText().toString().trim())) {
                        if (!TextUtils.isEmpty(passwordET.getText().toString().trim())) {

                            if (AppConstants.ISDRIVER) {
                                SharedPreferences rem_prefs = getSharedPreferences(getPackageName() + "REM_D", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor_d = rem_prefs.edit();
                                editor_d.putString("PASS", passwordET.getText().toString().trim());
                                editor_d.putString("EMAIL", emailET.getText().toString().trim());
                                editor_d.commit();
                            } else {
                                SharedPreferences rem_prefs = getSharedPreferences(getPackageName() + "REM_C", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor_c = rem_prefs.edit();
                                editor_c.putString("PASS", passwordET.getText().toString().trim());
                                editor_c.putString("EMAIL", emailET.getText().toString().trim());
                                editor_c.commit();

                            }

                        }
                    }
                } else {
                    if (AppConstants.ISDRIVER) {
                        SharedPreferences settings = getApplicationContext()
                                .getSharedPreferences(getPackageName() + "REM_D",
                                        Context.MODE_PRIVATE);
                        settings.edit().clear().commit();
                    } else {
                        SharedPreferences settings = getApplicationContext()
                                .getSharedPreferences(getPackageName() + "REM_C",
                                        Context.MODE_PRIVATE);
                        settings.edit().clear().commit();
                    }

                }


            }
        });

        loadingDialog = new LoadingDialog(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register_imageView:

                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.login_imageView:
                if (checkValidations()) {
                    if (AppConstants.ISDRIVER) {
                        GPS();
//                        DrawerClick.intentCall(this, HomeActivity.class);
                    } else {

                        doLogin(emailET.getText().toString().trim(), passwordET.getText().toString().trim(), "customer");
//                        Intent intent1 = new Intent(this, FindDriversActivity.class);
//                        startActivity(intent1);

                    }
//                    Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();

                }
                break;
            case R.id.forgetpassword_imageView:
                Intent intent1 = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent1);

                break;
        }
    }

    public boolean checkValidations() {

        if (!Validations.emailCheck(emailET.getText().toString().trim()) && !Validations.strslength(passwordET.getText().toString().trim())) {

            emailET.setError("Please enter valid email address.");
            passwordET.setError("Please enter password.");
            return false;
        } else if (!Validations.emailCheck(emailET.getText().toString().trim())) {
            emailET.setError("Please enter valid email address.");
            return false;

        } else if (!Validations.strslength(passwordET.getText().toString().trim())) {
            passwordET.setError("Please enter password.");
            return false;
        } else {

            return true;

        }
    }

    public void doLogin(final String email, final String password, final String type) {

        loadingDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("LOGIN", response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int message_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (message_code == 1) {

                        JSONArray jsonArray = jobj.getJSONArray("response");
                        int user_id = jsonArray.getJSONObject(0).getInt("user_id");
                        String name = jsonArray.getJSONObject(0).getString("name");
                        String phone = jsonArray.getJSONObject(0).getString("phone");
                        String address = jsonArray.getJSONObject(0).getString("address");
                        String user_type = jsonArray.getJSONObject(0).getString("user_type");
                        String password = jsonArray.getJSONObject(0).getString("password");
                        String email_address = jsonArray.getJSONObject(0).getString("email_address");
                        String profile_photo = jsonArray.getJSONObject(0).getString("profile_photo");
//                        String status = jsonArray.getJSONObject(0).getString("status");
//                        String token_id = jsonArray.getJSONObject(0).getString("token_id");
//                        String created_date = jsonArray.getJSONObject(0).getString("created_date");
//                        String verification_code = jsonArray.getJSONObject(0).getString("verification_code");

                        //check selected uer and get user are same type
                        if (type.equalsIgnoreCase(user_type)) {
                            if (type.equalsIgnoreCase("driver")) {

                                saveLoginData(user_id, name, phone, address, user_type, password, email_address, profile_photo);
                                DrawerClick.intentCall(LoginActivity.this, HomeActivity.class);
                            } else {
                                saveLoginData(user_id, name, phone, address, user_type, password, email_address, profile_photo);
                                Intent intent1 = new Intent(LoginActivity.this, FindDriversActivity.class);
                                startActivity(intent1);
                            }
                            ToastUlility.show(LoginActivity.this, message);
                        } else {
                            ToastUlility.show(LoginActivity.this, "Your login credentials are not correct, Please try to login again.");
                        }

                    } else {
                        ToastUlility.show(LoginActivity.this, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.EMAIL_ID, email);
                params.put(AppParams.PASSWORD, password);
//                params.put(AppParams.USER_TYPE, type);
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        String request_tag = "user_login";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void saveLoginData(int user_id, String name, String phone,
                              String address, String user_type, String password,
                              String email_address, String profile_photo) {
        editor.putInt("USER_ID", user_id);
        editor.putString("NAME", name);
        editor.putString("PHONE", phone);
        editor.putString("ADDRESS", address);
        editor.putString("USER_TYPE", user_type);
        editor.putString("EMAIL_ID", email_address);
        editor.putString("PROFILE_PHOTO", profile_photo);

        editor.commit();
    }

    public void GPS() {

        gps = new TrackGPS(LoginActivity.this);


        if (gps.canGetLocation()) {


            longitude = gps.getLongitude();
            latitude = gps.getLatitude();

            if (latitude != 0 && longitude != 0) {
                doLogin(emailET.getText().toString().trim(), passwordET.getText().toString().trim(), "driver");
//                getAddress(latitude, longitude);
            } else {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
//                ToastUlility.show(CallActivity.this, "Please restart app and try again.");
            }
//
        } else {
            showSettingsAlert();
        }
    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(LoginActivity.this);


        alertDialog.setTitle("GPS Not Enabled");

        alertDialog.setMessage("Do you wants to turn On GPS");


        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 101);
            }
        });


        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        alertDialog.show();
    }
}
