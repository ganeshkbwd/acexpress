package hnweb.com.acexpress.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        editor = prefs.edit();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.customer_imageView:
                AppConstants.ISDRIVER = true;
                goLogin();
//                Toast.makeText(this, "Coming soon....", Toast.LENGTH_SHORT).show();
                break;
            case R.id.driver_imageView:
                AppConstants.ISDRIVER = false;
                goLogin();
//                goLogin();
                break;
        }
    }

    public void goLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

    }
}
