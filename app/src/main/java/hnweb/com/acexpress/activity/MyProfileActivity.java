package hnweb.com.acexpress.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.utility.BackPress;
import hnweb.com.acexpress.utility.DrawerClick;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    Bundle bundle;
    Button changePasswordBTN, editProfileBTN;
    TextView myPaymentTV, nameTV, emailTV, addressTV, phoneTV;
    SharedPreferences sharedPreferences;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        bundle = getIntent().getExtras();
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        setToolbarDrawer();


    }

    public void init() {
        changePasswordBTN = (Button) findViewById(R.id.changePasswordBTN);
        editProfileBTN = (Button) findViewById(R.id.editProfileBTN);
        nameTV = (TextView) findViewById(R.id.nameTV);
        emailTV = (TextView) findViewById(R.id.emailTV);
        addressTV = (TextView) findViewById(R.id.addressTV);
        phoneTV = (TextView) findViewById(R.id.phoneTV);
        imageView = (ImageView) findViewById(R.id.imageView);


        if (!sharedPreferences.getString("PROFILE_PHOTO","").equalsIgnoreCase("")){
            Glide.with(MyProfileActivity.this).load(sharedPreferences.getString("PROFILE_PHOTO","")).override(150, 150).into(imageView);
        }
        nameTV.setText(sharedPreferences.getString("NAME", ""));
        emailTV.setText(sharedPreferences.getString("EMAIL_ID", ""));
        addressTV.setText(sharedPreferences.getString("ADDRESS", ""));
        phoneTV.setText(sharedPreferences.getString("PHONE", ""));

    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        if (AppConstants.ISDRIVER) {
            myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
            myPaymentTV.setText("MY PAYMENT");
        } else {
            myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
            myPaymentTV.setText("MY ORDER");
        }

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        init();

        if (bundle.getString("ComeFrom").equalsIgnoreCase("Notification")) {
            getSupportActionBar().setTitle("Driver Details");
            changePasswordBTN.setVisibility(View.GONE);
            editProfileBTN.setVisibility(View.GONE);
        } else {
            getSupportActionBar().setTitle("My Profile");
            changePasswordBTN.setVisibility(View.VISIBLE);
            editProfileBTN.setVisibility(View.VISIBLE);
        }

        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", null));

    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changePasswordBTN:
                DrawerClick.intentCall(this, ChangePasswordActivity.class);
                break;
            case R.id.editProfileBTN:
                DrawerClick.intentCall(this, EditProfileActivity.class);
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        BackPress.onBack(this);
    }
}
