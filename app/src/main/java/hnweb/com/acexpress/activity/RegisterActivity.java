package hnweb.com.acexpress.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.utility.CheckConnectivity;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;
import hnweb.com.acexpress.utility.Validations;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    EditText edit_name, edit_email, edit_location, edit_password, edit_phone, edit_repassword;
    String name, email, location, password, phone, repassword;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_email = (EditText) findViewById(R.id.edit_email);
        edit_location = (EditText) findViewById(R.id.edit_location);
        edit_password = (EditText) findViewById(R.id.edit_password);
        edit_repassword = (EditText) findViewById(R.id.edit_repassword);
        edit_phone = (EditText) findViewById(R.id.edit_phone);
        loadingDialog = new LoadingDialog(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signup_imageView:

                if (checkValid()) {
//                    ToastUlility.show(this,"Success");
//                    thanksDialog(4, "mike222taylor@gmail.com");
                    if (CheckConnectivity.checkInternetConnection(RegisterActivity.this)) {
                        doRegistration();
                    }
//                    thanksDialog();
                }
                // HIDE KEYBOARD
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//
//                FormEditText[] allFields = {edit_name, edit_email, edit_location, edit_password, edit_repassword, edit_phone};
//
//
//                boolean allValid = true;
//                for (FormEditText field : allFields) {
//                    allValid = field.testValidity() && allValid;
//                }
                break;
            case R.id.login_imageView:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public void getData() {

        name = edit_name.getText().toString().trim();
        email = edit_email.getText().toString().trim();
        location = edit_location.getText().toString().trim();
        phone = edit_phone.getText().toString().trim();
        password = edit_password.getText().toString().trim();
        repassword = edit_repassword.getText().toString().trim();
    }

    public boolean checkValid() {
        getData();
        if (!Validations.strslength(name) && !Validations.emailCheck(email) && !Validations.strslength(phone) && !Validations.strslength(password) && !Validations.confirmPass(password, repassword)) {

            edit_name.setError("Please enter name.");
            edit_name.requestFocus();
            edit_email.setError("Please enter valid email address.");
            edit_phone.setError("Please enter phone number.");
            edit_location.setError("Please enter address.");
            edit_password.setError("Please enter password.");
            edit_repassword.setError("Password and confirm password not match.");
            return false;
        } else if (!Validations.strslength(name)) {
            edit_name.setError("Please enter name.");
            return false;
        } else if (!Validations.emailCheck(email)) {
            edit_email.setError("Please enter valid email address.");
            return false;
        } else if (!Validations.strslength(phone) && !Patterns.PHONE.matcher(phone).matches()) {
            edit_phone.setError("Please enter valid phone number.");
            return false;
        } else if (!Validations.strslength(location)) {
            edit_location.setError("Please enter address.");
            return false;
        } else if (!Validations.strslength(password)) {
            edit_password.setError("Please enter password.");
            return false;
        } else if (!Validations.confirmPass(password, repassword)) {
            edit_repassword.setError("Password and confirm password not match.");
            return false;
        } else {
            return true;
        }

    }


    public void thanksDialog(final int user_id, final String email_address) {
        final Dialog settingsDialog = new Dialog(RegisterActivity.this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.register_dialog
                , null));
        settingsDialog.setCancelable(false);
        final Button upgradeBTN = (Button) settingsDialog.findViewById(R.id.verifyBTN);
        final EditText verifyET = (EditText) settingsDialog.findViewById(R.id.verifyET);

        upgradeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Validations.strslength(verifyET.getText().toString().trim())) {
                    doVerification(user_id, email_address, verifyET.getText().toString().trim());
                    settingsDialog.dismiss();
                } else {
                    verifyET.setError("Please enter verification code.");
                }

            }
        });

        settingsDialog.show();
    }

    public void doRegistration() {
        loadingDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.REGISTER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
                ToastUlility.show(RegisterActivity.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (msg_code == 1) {
                        int user_id = jobj.getInt("user_id");
                        String name = jobj.getString("name");
                        String email_address = jobj.getString("email_address");


                        ToastUlility.show(RegisterActivity.this, message);
                        thanksDialog(user_id, email_address);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.NAME, name);
                params.put(AppParams.PHONE, phone);
                params.put(AppParams.EMAIL_ID, email);
                params.put(AppParams.ADDRESS, location);
                if (AppConstants.ISDRIVER) {
                    params.put(AppParams.USER_TYPE, "driver");
                } else {
                    params.put(AppParams.USER_TYPE, "customer");
                }
                params.put(AppParams.PASSWORD, password);
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        // Add the request to the RequestQueue.
        String request_tag = "user_registration";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void doVerification(final int user_id, final String email_address, final String verifyCode) {
        loadingDialog.show();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.VERIFY_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("VERIFY RESPONSE", response.toString());

                try {
                    JSONObject jobj = new JSONObject(response);
                    int message_oode = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (message_oode == 1) {
                        int user_id = jobj.getInt("user_id");
                        String email_address = jobj.getString("email_address");

                        if (loadingDialog.isShowing()) {
                            loadingDialog.dismiss();
                        }
                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    ToastUlility.show(RegisterActivity.this, message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.USER_ID, String.valueOf(user_id));
                params.put(AppParams.EMAIL_ID, email_address);
                params.put(AppParams.VERIFICATION_CODE, verifyCode);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "user_verification";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }
}
