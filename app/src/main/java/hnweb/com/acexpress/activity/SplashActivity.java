package hnweb.com.acexpress.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.customer.FindDriversActivity;
import hnweb.com.acexpress.activity.driver.HomeActivity;
import hnweb.com.acexpress.utility.DrawerClick;
import hnweb.com.acexpress.utility.PermissionUtility;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Intent bundle;
    SharedPreferences sharedPreferences;
    private PermissionUtility putility;
    private ArrayList<String> permission_list;
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        editor = prefs.edit();
        permissions();

//        System.out.println(getDate(82233213123L, "dd/MM/yyyy hh:mm:ss a"));
//        try {
//            getSupportActionBar().hide();
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//        ImageButton getstarted_button = (ImageButton) findViewById(R.id.getstarted_button);
//        getstarted_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(isGooglePlayServicesAvailable(SplashActivity.this))
//                {
//                    goNextScreen();
//                }
//
//            }
//        });
//get Intent
//        bundle = getIntent();
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    private void goNextScreen() {
        if (sharedPreferences.getInt("USER_ID", 0) != 0) {

            if (sharedPreferences.getString("USER_TYPE", "").equalsIgnoreCase("driver")) {
                DrawerClick.intentCall(SplashActivity.this, HomeActivity.class);

            } else if (sharedPreferences.getString("USER_TYPE", "").equalsIgnoreCase("customer")) {
                Intent intent1 = new Intent(SplashActivity.this, FindDriversActivity.class);
                startActivity(intent1);
            } else {
                Intent in = new Intent(this, MainActivity.class);
//                in.putExtras(bundle);
                startActivity(in);
                finish();
            }
        } else {
            Intent in = new Intent(this, MainActivity.class);
//                in.putExtras(bundle);
            startActivity(in);
            finish();
        }
    }

    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (isGooglePlayServicesAvailable(SplashActivity.this)) {
            goNextScreen();
        }
    }

    public void permissions() {
        putility = new PermissionUtility(this);
        permission_list = new ArrayList<String>();
        permission_list.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permission_list.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permission_list.add(android.Manifest.permission.INTERNET);
        permission_list.add(android.Manifest.permission.ACCESS_WIFI_STATE);
        permission_list.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
        permission_list.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permission_list.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        permission_list.add(android.Manifest.permission.READ_CONTACTS);
        permission_list.add(android.Manifest.permission.CAMERA);
        permission_list.add(android.Manifest.permission.SEND_SMS);
        permission_list.add(android.Manifest.permission.READ_SMS);
        permission_list.add(android.Manifest.permission.READ_CONTACTS);
        permission_list.add(android.Manifest.permission.RECEIVE_SMS);
        permission_list.add(android.Manifest.permission.BROADCAST_SMS);
        permission_list.add(android.Manifest.permission.READ_PHONE_STATE);

        putility.setListner(new PermissionUtility.OnPermissionCallback() {
            @Override
            public void OnComplete(boolean is_granted) {
                Log.i("OnPermissionCallback", "is_granted = " + is_granted);
                if (is_granted) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//go next

//                            goNextScreen();
                        }
                    }, 3000);

                } else {
                    putility.checkPermission(permission_list);
                }
            }
        });


        putility.checkPermission(permission_list);
    }


}
