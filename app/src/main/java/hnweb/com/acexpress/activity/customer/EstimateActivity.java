package hnweb.com.acexpress.activity.customer;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.models.ViewEstimates;
import hnweb.com.acexpress.utility.DrawerClick;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;

public class EstimateActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView myPaymentTV;
    SharedPreferences sharedPreferences;
    //    ArrayList<ViewEstimates> viewEstimatesArrayList;
    TextView totalCostTV, fuelChargeTV, serviceCostTV, distanceDurationTV, milageCostTV,
            weightTV, dimensionsTV, deliveryAddressTV, pickupAddressTV, pickupTimeTV, pickupDateTV;
    ViewEstimates viewEstimates;
    LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimate);

        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        viewEstimates = (ViewEstimates) getIntent().getExtras().getSerializable("ESTIMATE");
        setToolbarDrawer();

        init();
        setData();
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("ESTIMATE");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
        myPaymentTV.setText("MY ORDER");

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", null));
    }

    public void init() {
        loadingDialog = new LoadingDialog(this);
        pickupDateTV = (TextView) findViewById(R.id.pickupDateTV);
        pickupTimeTV = (TextView) findViewById(R.id.pickupTimeTV);
        pickupAddressTV = (TextView) findViewById(R.id.pickupAddressTV);
        deliveryAddressTV = (TextView) findViewById(R.id.deliveryAddressTV);
        dimensionsTV = (TextView) findViewById(R.id.dimensionsTV);
        weightTV = (TextView) findViewById(R.id.weightTV);
        milageCostTV = (TextView) findViewById(R.id.milageCostTV);
        distanceDurationTV = (TextView) findViewById(R.id.distanceDurationTV);
        serviceCostTV = (TextView) findViewById(R.id.serviceCostTV);
        fuelChargeTV = (TextView) findViewById(R.id.fuelChargeTV);
        totalCostTV = (TextView) findViewById(R.id.totalCostTV);

    }

    public void setData() {
        pickupDateTV.setText(viewEstimates.getPickupdate());
        pickupTimeTV.setText(viewEstimates.getPicktime());
        pickupAddressTV.setText(viewEstimates.getPickupaddress());
        deliveryAddressTV.setText(viewEstimates.getDeliveryaddress());
        dimensionsTV.setText(viewEstimates.getDimension());
        weightTV.setText(viewEstimates.getWeight());
        milageCostTV.setText(viewEstimates.getMileage_cost());
        distanceDurationTV.setText(viewEstimates.getDistance() + "miles" + "(" + viewEstimates.getDuration() + ")");
//        serviceCostTV.setText(viewEstimates.get);
        fuelChargeTV.setText(viewEstimates.getFuel_charges());
        totalCostTV.setText(viewEstimates.getTotal_charges());


    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendRequestBTN:
                if (viewEstimates != null) {
                    sendRequestService();
                }

                break;
        }
    }

    public void requestDialog() {
        final Dialog settingsDialog = new Dialog(EstimateActivity.this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setCancelable(false);
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.send_request_dialog
                , null));
        settingsDialog.setCancelable(true);
        final Button upgradeBTN = (Button) settingsDialog.findViewById(R.id.upgradeBTN);


        upgradeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EstimateActivity.this, MyOrdersActivity.class);
                startActivity(intent);
                finish();
                settingsDialog.dismiss();
            }
        });

        settingsDialog.show();
    }

    public void sendRequestService() {
        loadingDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.SEND_REQUEST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("RESPONSE API", response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (msg_code == 1) {
                        requestDialog();
                        ToastUlility.show(EstimateActivity.this, message);
                    } else {
                        ToastUlility.show(EstimateActivity.this, message);
                    }
                } catch (Exception e) {

                }
                loadingDialog.dismiss();
//                requestDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingDialog.dismiss();
//                String reason = AppUtils.getVolleyError(getActivity(), error);
//                showAlertDialog(reason);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.ESTIMATE_ID, viewEstimates.getEstimate_id());
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        String request_tag = "send_request";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        BackPress.onBack(this);
        Intent intent = new Intent(this,RequestFormActivity.class);
        startActivity(intent);
        finish();
    }
}
