package hnweb.com.acexpress.activity.customer;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.fragments.customer.AllDriversFragment;
import hnweb.com.acexpress.fragments.customer.ByLocationFragment;
import hnweb.com.acexpress.utility.BackPress;
import hnweb.com.acexpress.utility.DrawerClick;

public class FindDriversActivity extends AppCompatActivity implements View.OnClickListener {


    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    public Button byLocationTV, allDriversTV;
    public TextView byTV, allTV, myPaymentTV;
    android.support.v4.app.FragmentManager fragmentManager;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    SharedPreferences sharedPreferences;
    public ImageView searchIV;
    public EditText searchET;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_drivers);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        AppConstants.MYORDERS = 0;
        setToolbarDrawer();
        init();
        setMyfragment();
//        GPS.checkGPS(this);

    }

    public void setMyfragment() {
        //add a fragment
        ByLocationFragment myFragment = new ByLocationFragment();
        replaceFragment(myFragment);
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchET = (EditText) toolbar.findViewById(R.id.searchET);
        searchIV.setOnClickListener(this);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
        myPaymentTV.setText("MY Requests");

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", null));

    }

    public void init() {
        byLocationTV = (Button) toolbar.findViewById(R.id.byLocationBTN);
        allDriversTV = (Button) toolbar.findViewById(R.id.allDriversBTN);
        byTV = (TextView) toolbar.findViewById(R.id.byTV);
        allTV = (TextView) toolbar.findViewById(R.id.allTV);

        byTV.setVisibility(View.VISIBLE);
        allTV.setVisibility(View.INVISIBLE);
        byLocationTV.setOnClickListener(this);
        allDriversTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.byLocationBTN:

//                searchIV.setVisibility(View.Vi);
                byTV.setVisibility(View.VISIBLE);
                allTV.setVisibility(View.INVISIBLE);
                AppConstants.MYORDERS = 0;
                ByLocationFragment hello1 = new ByLocationFragment();
                replaceFragment(hello1);


                break;
            case R.id.allDriversBTN:
//                searchIV.setVisibility(View.GONE);
                byTV.setVisibility(View.INVISIBLE);
                allTV.setVisibility(View.VISIBLE);
                AppConstants.MYORDERS = 0;
                AllDriversFragment hello = new AllDriversFragment();
                replaceFragment(hello);

                break;
            case R.id.searchIV:
//                if (searchET.getVisibility() == View.VISIBLE){
//                    if (searchET.getText().toString().trim().length()>0){
//                        GeocodingLocation locationAddress = new GeocodingLocation();
//                        locationAddress.getAddressFromLocation(searchET.getText().toString().trim(),
//                                getApplicationContext(), new FindDriversActivity.GeocoderHandler());
//                    }else {
//                        byLocationTV.setVisibility(View.VISIBLE);
//                        allDriversTV.setVisibility(View.VISIBLE);
//                        byTV.setVisibility(View.VISIBLE);
//                        allTV.setVisibility(View.INVISIBLE);
//                        searchET.setVisibility(View.GONE);
//                    }
//
//                }else {
//                    byLocationTV.setVisibility(View.GONE);
//                    allDriversTV.setVisibility(View.GONE);
//                    byTV.setVisibility(View.VISIBLE);
//                    allTV.setVisibility(View.INVISIBLE);
//                    searchET.setVisibility(View.VISIBLE);
//                }

                break;
        }
    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }



//    public class GeocoderHandler extends Handler {
//        @Override
//        public void handleMessage(Message message) {
//            String locationAddress;
//            switch (message.what) {
//                case 1:
//                    Bundle bundle = message.getData();
//                    locationAddress = bundle.getString("address");
//                    break;
//                default:
//                    locationAddress = null;
//            }
//
//
//            String[] latLong = locationAddress.split(",");
//            CameraUpdate center =
//                    CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(latLong[0]),
//                            Double.parseDouble(latLong[1])));
//            CameraUpdate zoom = CameraUpdateFactory.zoomTo(07);
//
//            ByLocationFragment bf = new ByLocationFragment();
//            bf.getLatLoing(latLong);
////            mMap.moveCamera(center);
////            mMap.animateCamera(zoom);
////            getAllTherapist(Double.parseDouble(latLong[0]), Double.parseDouble(latLong[1]));
//
//        }
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu, menu);
//        final MenuItem item = menu.findItem(R.id.action_search);
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
////        searchView.setOnQueryTextListener(this);
//
//        MenuItemCompat.setOnActionExpandListener(item,
//                new MenuItemCompat.OnActionExpandListener() {
//                    @Override
//                    public boolean onMenuItemActionCollapse(MenuItem item) {
//// Do something when collapsed
////                        adapter.setFilter(mCountryModel);
//                        return true; // Return true to collapse action view
//                    }
//
//                    @Override
//                    public boolean onMenuItemActionExpand(MenuItem item) {
//// Do something when expanded
//                        return true; // Return true to expand action view
//                    }
//                });
//    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        BackPress.onBack(FindDriversActivity.this);
    }


    //
}
