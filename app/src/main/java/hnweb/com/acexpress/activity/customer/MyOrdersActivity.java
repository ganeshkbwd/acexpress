package hnweb.com.acexpress.activity.customer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.fragments.customer.CompletedRequestFragment;
import hnweb.com.acexpress.fragments.customer.OngoingRequestFragment;
import hnweb.com.acexpress.fragments.customer.PendingRequestFragment;
import hnweb.com.acexpress.models.CustomerRequestInfo;
import hnweb.com.acexpress.utility.BackPress;
import hnweb.com.acexpress.utility.DrawerClick;

public class MyOrdersActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView prTV, orTV, crTV, myPaymentTV;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        AppConstants.MYORDERS = 1;
        setToolbarDrawer();
        setMyfragment();

    }

    public void setMyfragment() {
        //add a fragment
        PendingRequestFragment myFragment = new PendingRequestFragment();
        replaceFragment(myFragment);
    }


    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
        myPaymentTV.setText("MY ORDER");

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        prTV = (TextView) toolbar.findViewById(R.id.prTV);
        orTV = (TextView) toolbar.findViewById(R.id.orTV);
        crTV = (TextView) toolbar.findViewById(R.id.crTV);

        prTV.setVisibility(View.VISIBLE);
        orTV.setVisibility(View.INVISIBLE);
        crTV.setVisibility(View.INVISIBLE);

        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", null));

    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pendingTV:
                prTV.setVisibility(View.VISIBLE);
                orTV.setVisibility(View.INVISIBLE);
                crTV.setVisibility(View.INVISIBLE);
                AppConstants.MYORDERS = 1;
                AppConstants.ORDER = 1;
                PendingRequestFragment hello1 = new PendingRequestFragment();
                replaceFragment(hello1);
                break;
            case R.id.ongoingTV:
                prTV.setVisibility(View.INVISIBLE);
                orTV.setVisibility(View.VISIBLE);
                crTV.setVisibility(View.INVISIBLE);
                AppConstants.MYORDERS = 1;
                AppConstants.ORDER = 2;
                OngoingRequestFragment hello = new OngoingRequestFragment();
                replaceFragment(hello);
                break;
            case R.id.completedTV:
                prTV.setVisibility(View.INVISIBLE);
                orTV.setVisibility(View.INVISIBLE);
                crTV.setVisibility(View.VISIBLE);
                AppConstants.MYORDERS = 1;
                AppConstants.ORDER = 3;
                CompletedRequestFragment hell = new CompletedRequestFragment();
                replaceFragment(hell);
                break;
        }
    }

    public void replaceFragmentWithParams(Fragment fragment, ArrayList<CustomerRequestInfo> your_array_list, int position,String status) {

        Bundle arguments = new Bundle();
        arguments.putInt("POSITION", position);
        arguments.putSerializable("LIST", your_array_list);
        arguments.putString("STATUS",status);

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragment.setArguments(arguments);
        fragmentTransaction.commit();
    }



    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        BackPress.onBack(this);
    }
}
