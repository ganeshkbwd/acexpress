package hnweb.com.acexpress.activity.customer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.models.ViewEstimates;
import hnweb.com.acexpress.utility.DateTimeDialog;
import hnweb.com.acexpress.utility.DrawerClick;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;
import hnweb.com.acexpress.utility.Validations;

public class RequestFormActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView myPaymentTV;
    EditText pickupDateET, pickupTimeET, pickupAddressET, deliveryAddressET, weightET, dimensionsET;
    RadioGroup vehicleTypeRG, timeRG;
    RadioButton carRB, vanRB, truckRB, sameDayRB, nextDayRB;
    String vehicleType = "", deliveryTime = "", pickupDate = "", pickupTime = "", pickupAddress = "", deliveryAddress = "", weight = "", dimensions = "";
    SharedPreferences sharedPreferences;
    String driver_id;
    private LoadingDialog loadingDialog;
    String[] totalDistance, totalDuration;
    ArrayList<ViewEstimates> viewEstimatesArrayList = new ArrayList<ViewEstimates>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_form);
        loadingDialog = new LoadingDialog(this);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        driver_id = getIntent().getExtras().getString("DRIVER_ID");
        setToolbarDrawer();
        init();
    }

    public void init() {
        pickupDateET = (EditText) findViewById(R.id.pickupDateET);
        pickupTimeET = (EditText) findViewById(R.id.pickupTimeET);
        pickupAddressET = (EditText) findViewById(R.id.pickupAddressET);
        deliveryAddressET = (EditText) findViewById(R.id.deliveryAddressET);
        weightET = (EditText) findViewById(R.id.weightET);
        dimensionsET = (EditText) findViewById(R.id.dimensionsET);
        vehicleTypeRG = (RadioGroup) findViewById(R.id.vehicleTypeRG);
//        timeRG = (RadioGroup) findViewById(R.id.timeRG);

        pickupDateET.setOnTouchListener(this);
        pickupTimeET.setOnTouchListener(this);
        vehicleTypeRG.setOnCheckedChangeListener(onVhicleRG);
//        timeRG.setOnCheckedChangeListener(onTimeRG);

    }

    public void getData() {

        pickupDate = pickupDateET.getText().toString().trim();
        pickupTime = pickupTimeET.getText().toString().trim();
        pickupAddress = pickupAddressET.getText().toString().trim();
        deliveryAddress = deliveryAddressET.getText().toString().trim();
        weight = weightET.getText().toString().trim();
        dimensions = dimensionsET.getText().toString().trim();


    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("REQUEST FORM");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
        myPaymentTV.setText("MY ORDER");

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", null));

    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.viewEstimateBTN:
                checkValid();
                break;

        }
    }

    public void checkValid() {

        getData();
        if (Validations.strslength(pickupDate) == false && Validations.strslength(pickupTime) == false &&
                Validations.strslength(pickupAddress) == false && Validations.strslength(deliveryAddress) == false
                && Validations.strslength(weight) == false && Validations.strslength(dimensions) == false
                && Validations.strslength(vehicleType) == false) {

            pickupDateET.setError("Please enter pickup date.");
            pickupDateET.requestFocus();
            pickupTimeET.setError("Please enter pickup date.");
            pickupAddressET.setError("Please enter pickup date.");
            deliveryAddressET.setError("Please enter pickup date.");
            weightET.setError("Please enter pickup date.");
            dimensionsET.setError("Please enter pickup date.");


        } else if (Validations.strslength(pickupDate) == false) {
            pickupDateET.setError("Please enter pickup date.");
            pickupDateET.requestFocus();

        } else if (Validations.strslength(pickupTime) == false) {
            pickupTimeET.setError("Please enter pickup date.");
            pickupTimeET.requestFocus();

        } else if (Validations.strslength(pickupAddress) == false) {
            pickupAddressET.setError("Please enter pickup date.");
            pickupAddressET.requestFocus();

        } else if (Validations.strslength(deliveryAddress) == false) {
            deliveryAddressET.setError("Please enter pickup date.");
            deliveryAddressET.requestFocus();

        } else if (Validations.strslength(weight) == false) {
            weightET.setError("Please enter pickup date.");
            weightET.requestFocus();

        } else if (Validations.strslength(dimensions) == false) {
            dimensionsET.setError("Please enter pickup date.");
            dimensionsET.requestFocus();

        } else if (Validations.strslength(vehicleType) == false) {
            ToastUlility.show(this, "Please select type of vehicle.");

        }
//        else if (Validations.strslength(deliveryTime) == false) {
//            ToastUlility.show(this, "Please select delivery time.");
//        }
        else {
            distanceUsingGAPI(pickupAddress, deliveryAddress, pickupDate, pickupTime, pickupAddress, deliveryAddress, weight, dimensions, vehicleType);
//            Log.e("DD", dds);
//            getEstimates(pickupDate, pickupTime, pickupAddress, deliveryAddress, weight, dimensions, vehicleType);
//            Intent intent = new Intent(this, EstimateActivity.class);
//            startActivity(intent);
        }

    }

    RadioGroup.OnCheckedChangeListener onVhicleRG = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            RadioButton radioButton = (RadioButton) radioGroup.findViewById(i);
            int index = radioGroup.indexOfChild(radioButton);
            switch (index) {
                case 0:
                    vehicleType = radioButton.getText().toString();
                    break;
                case 1:
                    vehicleType = radioButton.getText().toString();
                    break;
                default:
//                    vehicleType = radioButton.getText().toString();
                    break;
            }

        }
    };

//    RadioGroup.OnCheckedChangeListener onTimeRG = new RadioGroup.OnCheckedChangeListener() {
//        @Override
//        public void onCheckedChanged(RadioGroup radioGroup, int i) {
//            RadioButton radioButton = (RadioButton) radioGroup.findViewById(i);
//            int index = radioGroup.indexOfChild(radioButton);
//            switch (index) {
//                case 0:
//                    deliveryTime = radioButton.getText().toString();
//                    break;
//                case 1:
//                    deliveryTime = radioButton.getText().toString();
//                    break;
//
//            }
//        }
//    };

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        DateTimeDialog dtd = new DateTimeDialog();
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

            switch (view.getId()) {
                case R.id.pickupDateET:

                    dtd.getDate(this, pickupDateET);
                    pickupDateET.setError(null);
                    break;
                case R.id.pickupTimeET:
                    dtd.getTime(this, pickupTimeET);
                    pickupTimeET.setError(null);
//                dtd.getTime(this);
                    break;
            }
            // Do what you want
            return true;
        }
        return false;
    }

    public void getEstimates(final String pickupDate, final String pickupTime,
                             final String pickupAddress, final String deliveryAddress,
                             final String weight, final String dimensions,
                             final String vehicleType, final String distance, final String duration) {
        loadingDialog.show();
//        final String[] dds = distanceUsingGAPI(pickupAddress, deliveryAddress);
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GET_ESTIMATES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
//                ToastUlility.show(All.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
//                    String message = jobj.getString("message");
                    if (msg_code == 1) {
                        viewEstimatesArrayList.clear();
                        JSONObject jarr = jobj.getJSONObject("response");
//                        for (int i = 0; i < jarr.length(); i++) {
                        ViewEstimates viewEstimates = new ViewEstimates();
                        viewEstimates.setEstimate_id(jarr.getString("estimate_id"));
                        viewEstimates.setPickupdate(jarr.getString("pickupdate"));
                        viewEstimates.setPicktime(jarr.getString("picktime"));
                        viewEstimates.setPickupaddress(jarr.getString("pickupaddress"));
                        viewEstimates.setDeliveryaddress(jarr.getString("deliveryaddress"));
                        viewEstimates.setWeight(jarr.getString("weight"));
                        viewEstimates.setDimension(jarr.getString("dimension"));
                        viewEstimates.setDistance(jarr.getString("distance"));
                        viewEstimates.setDuration(jarr.getString("duration"));
                        viewEstimates.setUserid_customer(jarr.getString("userid_customer"));
                        viewEstimates.setUserid_driver(jarr.getString("userid_driver"));
                        viewEstimates.setType_of_vehicle(jarr.getString("type_of_vehicle"));
                        viewEstimates.setDelivery_time(jarr.getString("delivery_time"));
                        viewEstimates.setRequest_status(jarr.getString("request_status"));
                        viewEstimates.setMileage_cost(jarr.getString("mileage_cost"));
                        viewEstimates.setFuel_charges(jarr.getString("fuel_charges"));
                        viewEstimates.setTotal_charges(jarr.getString("total_charges"));

//                            viewEstimatesArrayList.add(viewEstimates);

//                            driversInfoArrayList.add(driversInfo);


//                        }

                        Intent intent = new Intent(RequestFormActivity.this, EstimateActivity.class);
                        intent.putExtra("ESTIMATE", viewEstimates);
                        startActivity(intent);
                        finish();

//                        AllDriversListAdapter dla = new AllDriversListAdapter(getActivity(), driversInfoArrayList);
//                        lv.setAdapter(dla);


                    } else {
                        String message = jobj.getString("message");
                        ToastUlility.show(RequestFormActivity.this, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(RequestFormActivity.this, "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.PICKUP_DATE, pickupDate);
                params.put(AppParams.PICKUP_TIME, pickupTime);
                params.put(AppParams.PICKUP_ADDRESS, pickupAddress);
                params.put(AppParams.DELIVERY_ADDRESS, deliveryAddress);
                params.put(AppParams.DIMENSION, dimensions);
                params.put(AppParams.WEIGHT, weight);
                params.put(AppParams.DRIVER_ID, driver_id);
                params.put(AppParams.DISTANCE, distance);
                params.put(AppParams.DURATION, duration);
                params.put(AppParams.VEHICLE_TYPE, vehicleType);
                params.put(AppParams.CUSTOMER_ID, String.valueOf(sharedPreferences.getInt("USER_ID", 0)));

                params.put(AppParams.DELIVERY_TIME, "d_time");
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        // Add the request to the RequestQueue.
        String request_tag = "requst_for_estimates";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void distanceUsingGAPI(final String resto_address, final String delivery_address, final String pickupDate, final String pickupTime, final String pickupAddress, final String deliveryAddress, final String weight, final String dimensions, final String vehicleType) {
        loadingDialog.show();
        String distance, duration;
        String rst_ad = resto_address.replaceAll(" ", "%20");
        String del_ad = delivery_address.replaceAll(" ", "%20");

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + rst_ad + "&destinations=" + del_ad + "&key=" + AppUrls.GOOGLE_DISTANCE_API_KEY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("RESPONSE API", response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    JSONArray jarr = jobj.getJSONArray("rows");


                    String distance = jarr.getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("distance").getString("text");
                    String duration = jarr.getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("duration").getString("text");


                    totalDistance = distance.split("\\s+");
                    String total_Distance_ = totalDistance[0].replaceAll(",", "");
//                    totalDuration = duration.split("\\s+");

                    getEstimates(pickupDate, pickupTime, pickupAddress, deliveryAddress, weight, dimensions, vehicleType, total_Distance_, duration);

                    Log.e("DDD", total_Distance_ + "--" + duration);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                loadingDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingDialog.dismiss();
//                String reason = AppUtils.getVolleyError(getActivity(), error);
//                showAlertDialog(reason);
                error.printStackTrace();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(RequestFormActivity.this);
        requestQueue.add(stringRequest);
//        String[] dd = new String[]{};
//        dd[0] = totalDistance[0];
//        dd[1] = totalDuration[0];

//        return totalDistance[0] + "=" + totalDuration[0];
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        BackPress.onBack(this);
        Intent intent = new Intent(this, FindDriversActivity.class);
        startActivity(intent);
        finish();
    }


}
