package hnweb.com.acexpress.activity.driver;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.koushikdutta.async.AsyncSocket;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.fragments.driver.DriverPendingRequestFragment;
import hnweb.com.acexpress.fragments.driver.NewRequestFragment;
import hnweb.com.acexpress.fragments.driver.OngoingRequestDriverFragment;
import hnweb.com.acexpress.models.OrdersInfo;
import hnweb.com.acexpress.utility.BackPress;
import hnweb.com.acexpress.utility.DrawerClick;
import hnweb.com.acexpress.utility.PermissionUtility;
import hnweb.com.acexpress.utility.SocketUtility;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, LocationListener {
    public Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView prqTV, nrqTV, myPaymentTV, orqTV;
    SharedPreferences sharedPreferences;
    private SocketUtility sutility;
    private OnReceiveDataListner date_received_listner;
    private boolean user_tcp_login;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private OnLocationChangelistner location_changed_listner;
    private PermissionUtility putility;
    public LinearLayout titleLL;

    public void setLocationChangedlistner(OnLocationChangelistner listner) {
        this.location_changed_listner = listner;
    }

    public void setDataReceivedlistner(OnReceiveDataListner listner) {
        this.date_received_listner = listner;
    }

    public boolean isUserLoginTCP() {
        return user_tcp_login;
    }

    public interface OnLocationChangelistner {
        public void OnLocationChanged(Location location);

    }

    public interface OnReceiveDataListner {
        public void OnLogin(boolean is_login);

        public void OnDataReceived(JSONObject jobj);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        setToolbarDrawer();
        socketUtility();
        this.location_changed_listner = null;
        this.date_received_listner = null;

        putility = new PermissionUtility(this);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        setMyfragment();
    }


    public void socketUtility() {
        sutility = new SocketUtility(this);
        sutility.setServerConfig(AppUrls.TCP_SERVER_ADDRESS, AppUrls.TCP_SERVER_PORT);
        sutility.setConnectionCallbackListner(new SocketUtility.OnConnectionListner() {


            @Override
            public void OnConnected(AsyncSocket clientSocket) {
                Log.i("SocketUtility", "OnConnected ");
            /*    JSONObject jobj = new JSONObject();
             boolean is_success = sutility.sendCommand(jobj);*/
            }

            @Override
            public void OnReceiveData(JSONObject obj) {
                new DataPostTask(obj).execute();
                Log.i("SocketUtility", "OnReceiveData obj= " + obj.toString());
            }

            @Override
            public void OnDisconnected() {
                Log.i("SocketUtility", "OnDisconnected");
            }

            @Override
            public void OnError() {
                Log.i("SocketUtility", "OnError");
            }
        });
    }

    public void setMyfragment() {
        //add a fragment
        NewRequestFragment myFragment = new NewRequestFragment();
//        NewRequestFragment myFragment = new NewRequestFragment();
        replaceFragment(myFragment);
    }

    @Override
    protected void onStart() {
        sutility.onStart();
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        sutility.onStop();
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (putility != null) {
            putility.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        startLocationUpdates();
        checkGPS();

    }

    private void checkGPS() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //check location setting
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdates();
                        Log.i("LSettingsStatusCodes", "SUCCESS");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                        builder.setTitle("Enable GPS");  // GPS not found
                        builder.setMessage("GPS Must be enabled"); // Want to enable?
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //      onBackPressed();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.setCancelable(false);
                        dialog.show();
                        Log.i("SettingsStatusCodes", "RESOLUTION_REQUIRED");
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("LSettingsStatusCodes", "SETTINGS_CHANGE_UNAVAILABLE");
                        break;
                }
            }
        });
    }

    private void startLocationUpdates() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public Location getLastLocation() {
        return mLastLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        //   Log.i("onLocationChanged", "location =="+location.toString());
        mLastLocation = location;
        if (location_changed_listner != null && location != null) {
            location_changed_listner.OnLocationChanged(location);
        }

//        if (sharedPreferences.getBoolean(AppConstants.DRIVER_AVALIABLITY_STATUS, false)) {
//            updateDriverPosition(location);
//        }
//        if (sharedPreferences.getBoolean(AppUrls.DRIVER_TRACKING_ENABLED, false)) {
        updateDriverTrackingPosition(location);
//        }

    }

    private void updateDriverTrackingPosition(Location location) {
        if (sutility != null && sutility.isConnected()) {
            JSONObject obj = new JSONObject();
            try {

                // JSONObject pobj = new JSONObject();
                JSONObject dobj = new JSONObject();
                dobj.put("customer_id", 2);
                dobj.put("driver_id", sharedPreferences.getInt("USER_ID", 0));
                dobj.put("latitude", location.getLatitude());
                dobj.put("longitude", location.getLongitude());
                dobj.put("accuracy", location.getAccuracy());
                dobj.put("altitude", location.getAltitude());
                dobj.put("bearing", location.getBearing());
                dobj.put("speed", location.getSpeed());
                obj.put("action", AppUrls.TCP_ACTION_UPDATE_DRIVER_TRACKING);
                obj.put("data", new JSONObject().put("tracking_info", dobj));
                Log.e("TRACKING UPDATE", dobj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sutility.sendCommand(obj);
        }
    }


    public SocketUtility getSocket() {
        return sutility;
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        titleLL = (LinearLayout) toolbar.findViewById(R.id.titleLL);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
        myPaymentTV.setText("MY PAYMENT");

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        prqTV = (TextView) toolbar.findViewById(R.id.prqTV);
        nrqTV = (TextView) toolbar.findViewById(R.id.nrqTV);
        orqTV = (TextView) toolbar.findViewById(R.id.orqTV);


        prqTV.setVisibility(View.INVISIBLE);
        nrqTV.setVisibility(View.VISIBLE);
        orqTV.setVisibility(View.INVISIBLE);

        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", null));
    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newReqBTN:
                prqTV.setVisibility(View.INVISIBLE);
                nrqTV.setVisibility(View.VISIBLE);
                orqTV.setVisibility(View.INVISIBLE);
                AppConstants.NEWREQUEST = "NEW";
                NewRequestFragment nrf = new NewRequestFragment();
                replaceFragment(nrf);

                break;
            case R.id.penReqBTN:
                prqTV.setVisibility(View.VISIBLE);
                nrqTV.setVisibility(View.INVISIBLE);
                orqTV.setVisibility(View.INVISIBLE);
                AppConstants.NEWREQUEST = "PENDING";
                DriverPendingRequestFragment prf = new DriverPendingRequestFragment();
                replaceFragment(prf);
                break;
            case R.id.ongoingReqBTN:
                prqTV.setVisibility(View.INVISIBLE);
                nrqTV.setVisibility(View.INVISIBLE);
                orqTV.setVisibility(View.VISIBLE);
                AppConstants.NEWREQUEST = "ONGOING";
                OngoingRequestDriverFragment orf = new OngoingRequestDriverFragment();
                replaceFragment(orf);
                break;
        }
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    public void replaceFragmentWithParams(android.support.v4.app.Fragment fragment, ArrayList<OrdersInfo> your_array_list, int position, String status) {
        Bundle argument = new Bundle();
        argument.putSerializable("LIST", (Serializable) your_array_list);
        argument.putInt("POSITION", position);
        argument.putString("STATUS", status);
//        argument.putInt("ESTIMAT", position);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragment.setArguments(argument);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        BackPress.onBack(this);
    }

    private class DataPostTask extends AsyncTask<Object, Object, JSONObject> {
        private final JSONObject inobj;

        public DataPostTask(JSONObject obj) {
            this.inobj = obj;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(JSONObject obj) {
            if (date_received_listner != null) {
                date_received_listner.OnDataReceived(obj);
            }
            try {
                String action = obj.getString("action");
                int status_code = obj.getInt("status_code");
                String message = obj.getString("message");
                if (action.equals(AppUrls.TCP_ACTION_CLIENT_CONNECTED)) {
                    JSONObject cobj = obj.getJSONObject("data").getJSONObject("client_info");
                    final String client_ip = cobj.getString("client_ip");
                    final int client_port = cobj.getInt("client_port");
                    doLoginTCP(client_ip, client_port);
                } else if (action.equals(AppUrls.TCP_ACTION_ACTION_USER_LOGIN)) {
                    if (status_code == 1) {
                        if (date_received_listner != null) {
                            user_tcp_login = true;
                            date_received_listner.OnLogin(true);
                        }
                    } else {
                        if (date_received_listner != null) {
                            user_tcp_login = false;
                            date_received_listner.OnLogin(false);
                        }
                    }
//showAlert();
                } else if (action.equals(AppUrls.TCP_ACTION_DRIVER_REQUEST_REPLY)) {
                    boolean success = (status_code == 1) ? true : false;
                    showAlert(success, message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            super.onPostExecute(obj);
        }

        @Override
        protected JSONObject doInBackground(Object... params) {
            return inobj;
        }
    }

    private void showAlert(boolean success, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (success) {
            builder.setTitle("Success");
        } else {
            builder.setTitle("Error");
        }


        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void doLoginTCP(String client_ip, int client_port) {
        String email = sharedPreferences.getString("EMAIL_ID", "");
        int user_type = 2;
//                sharedPreferences.getInt(AppUrls.USER_TYPE, 0);
        if (sutility != null && sutility.isConnected()) {
            JSONObject obj = new JSONObject();
            try {

                // JSONObject pobj = new JSONObject();
                JSONObject dobj = new JSONObject();
                dobj.put("email", email);
                dobj.put("user_type", "driver");
                dobj.put("ip_address", client_ip);
                dobj.put("port", client_port);
                obj.put("action", AppUrls.TCP_ACTION_ACTION_USER_LOGIN);
                Log.e("DOBJ", dobj.toString());
                obj.put("data", new JSONObject().put("user_info", dobj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sutility.sendCommand(obj);
        }
    }
}
