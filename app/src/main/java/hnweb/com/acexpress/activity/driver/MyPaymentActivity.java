package hnweb.com.acexpress.activity.driver;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.fragments.driver.PendingPaymentFragment;
import hnweb.com.acexpress.fragments.driver.ReceivedPaymentFragment;
import hnweb.com.acexpress.utility.BackPress;
import hnweb.com.acexpress.utility.DrawerClick;

public class MyPaymentActivity extends AppCompatActivity implements View.OnClickListener{

    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView prqTV, nrqTV, myPaymentTV;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_payment);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        setToolbarDrawer();


    }

    public void setMyfragment() {
        //add a fragment
        PendingPaymentFragment myFragment = new PendingPaymentFragment();
//        NewRequestFragment myFragment = new NewRequestFragment();
        replaceFragment(myFragment);
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
        myPaymentTV.setText("MY PAYMENT");

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();


        prqTV = (TextView) toolbar.findViewById(R.id.prqTV);
        nrqTV = (TextView) toolbar.findViewById(R.id.nrqTV);


        prqTV.setVisibility(View.INVISIBLE);
        nrqTV.setVisibility(View.VISIBLE);
        setMyfragment();

        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", null));

    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newReqBTN:
                prqTV.setVisibility(View.INVISIBLE);
                nrqTV.setVisibility(View.VISIBLE);

                PendingPaymentFragment nrf = new PendingPaymentFragment();
                replaceFragment(nrf);

                break;
            case R.id.penReqBTN:
                prqTV.setVisibility(View.VISIBLE);
                nrqTV.setVisibility(View.INVISIBLE);
//                AppConstants.NEWREQUEST = false;
                ReceivedPaymentFragment prf = new ReceivedPaymentFragment();
                replaceFragment(prf);
                break;
        }
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        BackPress.onBack(this);
    }
}
