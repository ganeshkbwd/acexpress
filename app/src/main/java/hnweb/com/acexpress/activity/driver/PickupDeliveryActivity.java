package hnweb.com.acexpress.activity.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.utility.DrawerClick;
import hnweb.com.acexpress.utility.GPS;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;

public class PickupDeliveryActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, HomeActivity.OnLocationChangelistner {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView myPaymentTV;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private Marker positionMarker;
    private Circle circleMarker;
    Button serviceBTN;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    SharedPreferences sharedPreferences;
    private LoadingDialog loadingDialog;
    String ESTIMATE_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup_delivery);

        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        loadingDialog = new LoadingDialog(this);
        ESTIMATE_ID = getIntent().getExtras().getString("ESTIMATE_ID");
        setToolbarDrawer();
        GPS.checkGPS(this);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        HomeActivity homeActivity = new HomeActivity();
        homeActivity.setLocationChangedlistner(this);


    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("PICKUP DELIVERY");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
        myPaymentTV.setText("MY PAYMENT");
//        myPaymentTV.setCompoundDrawablesWithIntrinsicBounds();
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
//        init();
        setMap();

        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", null));

        if (AppConstants.NEWREQUEST.equalsIgnoreCase("ONGOING")) {
            serviceBTN.setText("Service completed");
        } else {
            serviceBTN.setText("start service");
        }

    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    public void setMap() {
        mapFragment = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map));
        mapFragment.getMapAsync(PickupDeliveryActivity.this);

        serviceBTN = (Button) findViewById(R.id.serviceBTN);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng myPosition;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);


        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            myPosition = new LatLng(latitude, longitude);


            LatLng coordinate = new LatLng(latitude, longitude);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 19);
            mMap.animateCamera(yourLocation);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.serviceBTN:
                if (serviceBTN.getText().toString().trim().equalsIgnoreCase("start service")) {
                    serviceBTN.setText("Service completed");
                    doServiceActions(ESTIMATE_ID, "ONGOING");
                    ToastUlility.show(this, "Service Started Successfully");
                } else {
                    serviceBTN.setText("start service");
                    doServiceActions(ESTIMATE_ID, "COMPLETED");
                    ToastUlility.show(this, "Service Completed Successfully");
                }

                break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        if (mLastLocation != null && mMap != null) {

            LatLng position = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16.0f));
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void doServiceActions(final String user_id, final String status) {
        loadingDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.START_COMPLETE_SERVICE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
//                ToastUlility.show(All.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
//                    String message = jobj.getString("message");
                    if (msg_code == 1) {

                        if (status.equals("COMPLETED")) {
                            Intent intent = new Intent(PickupDeliveryActivity.this, MyPaymentActivity.class);
                            startActivity(intent);
                            finish();
                        }


                    } else {
                        String message = jobj.getString("message");
                        ToastUlility.show(PickupDeliveryActivity.this, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(PickupDeliveryActivity.this, "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.ESTIMATE_ID, user_id);
                params.put(AppParams.REQUEST_STATUS, status);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };


        // Add the request to the RequestQueue.
        String request_tag = "new_orders";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }


    @Override
    public void OnLocationChanged(Location location) {
        updateMarker(location);
    }

    private void updateMarker(Location location) {
        //  Log.i("TrackView","update Marker called"+location.toString());

        if (mMap != null) {
            if (positionMarker == null) {
                MarkerOptions positionMarkerOptions = new MarkerOptions();
                positionMarkerOptions.draggable(false);
                positionMarkerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_action_pick_map);
                positionMarkerOptions.icon(icon);
                positionMarkerOptions.flat(true);
                positionMarkerOptions.anchor(0.5f, 0.5f);
                positionMarker = mMap.addMarker(positionMarkerOptions);


                CircleOptions circleMarkerOptions = new CircleOptions();
                circleMarkerOptions.center(new LatLng(location.getLatitude(), location.getLongitude()));
                circleMarkerOptions.radius(location.getAccuracy());
                circleMarkerOptions.fillColor(Color.parseColor("#40E52F2F"));
                circleMarkerOptions.strokeWidth(1.0f);
                circleMarkerOptions.strokeColor(Color.parseColor("#E52F2F"));
                circleMarker = mMap.addCircle(circleMarkerOptions);
            } else {
                Log.i("GPS", "bearing angle= " + location.getBearing());
                positionMarker.setRotation(location.getBearing());
                positionMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
                circleMarker.setRadius(location.getAccuracy());
                circleMarker.setCenter(new LatLng(location.getLatitude(), location.getLongitude()));
            }

        }
    }
}
