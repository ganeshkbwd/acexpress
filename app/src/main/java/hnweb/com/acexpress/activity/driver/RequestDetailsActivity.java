package hnweb.com.acexpress.activity.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.models.OrdersInfo;
import hnweb.com.acexpress.utility.DrawerClick;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;

public class RequestDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    Button acceptBTN, declineBTN;
    TextView myPaymentTV;
    SharedPreferences sharedPreferences;
    ArrayList<OrdersInfo> ordersInfoArrayList;
    int position;
    private LoadingDialog loadingDialog;


    TextView customerNameTV, dateTV, timeTV, pAddressTV, dAddressTV, dimensionsTV, weightTV;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        loadingDialog = new LoadingDialog(this);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        ordersInfoArrayList = (ArrayList<OrdersInfo>) getIntent().getExtras().getSerializable("LIST");
        position = getIntent().getExtras().getInt("POSITION");
        setToolbarDrawer();


        customerNameTV.setText(ordersInfoArrayList.get(position).getName());
        dateTV.setText(ordersInfoArrayList.get(position).getPickupdate());
        timeTV.setText(ordersInfoArrayList.get(position).getPicktime());
        pAddressTV.setText(ordersInfoArrayList.get(position).getPickupaddress());
        dAddressTV.setText(ordersInfoArrayList.get(position).getDeliveryaddress());
        dimensionsTV.setText(ordersInfoArrayList.get(position).getDimension());
        weightTV.setText(ordersInfoArrayList.get(position).getWeight());

    }

    public void init() {

        customerNameTV = (TextView) findViewById(R.id.customerNameTV);
        imageView = (ImageView) findViewById(R.id.imageView);
        dateTV = (TextView) findViewById(R.id.dateTV);
        timeTV = (TextView) findViewById(R.id.timeTV);
        pAddressTV = (TextView) findViewById(R.id.pAddressTV);
        dAddressTV = (TextView) findViewById(R.id.dAddressTV);
        dimensionsTV = (TextView) findViewById(R.id.dimensionsTV);
        weightTV = (TextView) findViewById(R.id.weightTV);

        acceptBTN = (Button) findViewById(R.id.acceptBTN);
        declineBTN = (Button) findViewById(R.id.declineBTN);

        if (AppConstants.NEWREQUEST.equalsIgnoreCase("NEW")) {
            acceptBTN.setVisibility(View.VISIBLE);
            declineBTN.setText("DECLINE");

        } else if (AppConstants.NEWREQUEST.equalsIgnoreCase("PENDING")) {
            acceptBTN.setVisibility(View.GONE);
            declineBTN.setText("VIEW MAP");
        } else if (AppConstants.NEWREQUEST.equalsIgnoreCase("ONGOING")) {
            acceptBTN.setVisibility(View.GONE);
            declineBTN.setText("VIEW MAP");
        }
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("REQUEST DETAILS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
        myPaymentTV.setText("MY PAYMENT");
//        myPaymentTV.setCompoundDrawablesWithIntrinsicBounds();
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        init();
        TextView use_name = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        ImageView proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        Glide.with(this).load(sharedPreferences.getString("PROFILE_PHOTO", null)).override(90, 90).into(proPicIV);

        use_name.setText(sharedPreferences.getString("NAME", ""));


    }

    public void onDrawerClick(View v) {

        DrawerClick.drawerClick(this, v.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptBTN:
                doAcceptDecline(sharedPreferences.getInt("USER_ID", 0), "ACCEPTED");
                ToastUlility.show(this, "Request Accepted....Under Development.....");
                break;
            case R.id.declineBTN:
                if (AppConstants.NEWREQUEST.equalsIgnoreCase("NEW")) {
                    doAcceptDecline(sharedPreferences.getInt("USER_ID", 0), "DECLINED");
                    ToastUlility.show(this, "Request Declined....Under Development....");
                } else {
                    Intent intent = new Intent(this, PickupDeliveryActivity.class);
                    intent.putExtra("ESTIMATE_ID", ordersInfoArrayList.get(position).getEstimate_id());
                    startActivity(intent);
                    finish();
                }

                break;
        }
    }

    public void doAcceptDecline(final int user_id, final String status) {
        loadingDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.ACCEPDECLINE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
//                ToastUlility.show(All.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (msg_code == 1) {

                        ToastUlility.show(RequestDetailsActivity.this, message);
                        Intent intent = new Intent(RequestDetailsActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
//                        String message = jobj.getString("message");
                        ToastUlility.show(RequestDetailsActivity.this, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(RequestDetailsActivity.this, "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.ESTIMATE_ID, ordersInfoArrayList.get(position).getEstimate_id());
                params.put(AppParams.REQUEST_STATUS, status);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };


        // Add the request to the RequestQueue.
        String request_tag = "new_orders";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
