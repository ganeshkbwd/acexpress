package hnweb.com.acexpress.adapter;

/**
 * Created by neha on 6/5/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.customer.RequestFormActivity;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.models.DriversInfo;
import hnweb.com.acexpress.utility.ToastUlility;

/**
 * Created by neha on 2/1/2017.
 */

public class AllDriversListAdapter extends RecyclerView.Adapter<AllDriversListAdapter.AllDriverListViewHolder> {
    Context context;
    List<DriversInfo> your_array_list;

    public AllDriversListAdapter(Activity activity, ArrayList<DriversInfo> your_array_list) {
        this.context = activity;
        this.your_array_list = your_array_list;
    }

    @Override
    public AllDriverListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.driver_list_item, null);
        AllDriverListViewHolder dlvh = new AllDriverListViewHolder(layoutView);

        return dlvh;
    }

    @Override
    public void onBindViewHolder(AllDriverListViewHolder holder, final int position) {
        holder.driverNameTV.setText(your_array_list.get(position).getName().toString());
        holder.addressTV.setText("Address : " + your_array_list.get(position).getAddress());
        if (TextUtils.isEmpty(your_array_list.get(position).getProfile_photo())) {

        } else {
            Glide.with(context).load(your_array_list.get(position).getProfile_photo()).into(holder.profileIV);
        }

        if (AppConstants.MYORDERS == 1) {
            holder.requestBTN.setText("VIEW DETAILS");
            holder.requestBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ViewDetailsFragment vf = new ViewDetailsFragment();
//                    ((FindDriversActivity) context).replaceFragment(vf);
                    ToastUlility.show(context, "Under Development..");
                }
            });

        } else {

            holder.requestBTN.setText("REQUEST");
            holder.requestBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, RequestFormActivity.class);
                    intent.putExtra("DRIVER_ID", your_array_list.get(position).getUser_id());
                    context.startActivity(intent);

                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return your_array_list.size();
    }


    public static class AllDriverListViewHolder extends RecyclerView.ViewHolder {
        public TextView driverNameTV, addressTV;
        public Button requestBTN;
        public ImageView profileIV;

        public AllDriverListViewHolder(View itemView) {
            super(itemView);
            driverNameTV = (TextView) itemView.findViewById(R.id.driverNameTV);
            requestBTN = (Button) itemView.findViewById(R.id.requestBTN);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            profileIV = (ImageView) itemView.findViewById(R.id.profileIV);
        }
    }
}

