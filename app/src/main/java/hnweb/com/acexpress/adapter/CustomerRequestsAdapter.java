package hnweb.com.acexpress.adapter;

/**
 * Created by neha on 6/16/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.customer.MyOrdersActivity;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.fragments.customer.ViewDetailsFragment;
import hnweb.com.acexpress.models.CustomerRequestInfo;

/**
 * Created by neha on 6/5/2017.
 */

/**
 * Created by neha on 2/1/2017.
 */

public class CustomerRequestsAdapter extends RecyclerView.Adapter<CustomerRequestsAdapter.AllDriverListViewHolder> {
    Context context;
    ArrayList<CustomerRequestInfo> your_array_list;
    String status;

    public CustomerRequestsAdapter(Activity activity, ArrayList<CustomerRequestInfo> your_array_list, String status) {
        this.context = activity;
        this.your_array_list = your_array_list;
        this.status = status;
    }

    @Override
    public AllDriverListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.driver_list_item, null);
        AllDriverListViewHolder dlvh = new AllDriverListViewHolder(layoutView);

        return dlvh;
    }

    @Override
    public void onBindViewHolder(AllDriverListViewHolder holder, final int position) {
        holder.driverNameTV.setText(your_array_list.get(position).getName().toString());
        holder.addressTV.setText("Address : " + your_array_list.get(position).getAddress());
        if (TextUtils.isEmpty(your_array_list.get(position).getProfile_photo())) {

        } else {
            Glide.with(context).load(your_array_list.get(position).getProfile_photo()).into(holder.profileIV);
        }

        if (AppConstants.MYORDERS == 1) {
            holder.requestBTN.setText("VIEW DETAILS");
            holder.requestBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ViewDetailsFragment vf = new ViewDetailsFragment();
                    ((MyOrdersActivity) context).replaceFragmentWithParams(vf, your_array_list, position,status);

                }
            });
//
        }
//        else {
//
//            holder.requestBTN.setText("REQUEST");
//            holder.requestBTN.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(context, RequestFormActivity.class);
//                    intent.putExtra("DRIVER_ID", your_array_list.get(position).getUser_id());
//                    context.startActivity(intent);
//
//                }
//            });
//        }


    }


    @Override
    public int getItemCount() {
        return your_array_list.size();
    }


    public static class AllDriverListViewHolder extends RecyclerView.ViewHolder {
        public TextView driverNameTV, addressTV;
        public Button requestBTN;
        public ImageView profileIV;

        public AllDriverListViewHolder(View itemView) {
            super(itemView);
            driverNameTV = (TextView) itemView.findViewById(R.id.driverNameTV);
            requestBTN = (Button) itemView.findViewById(R.id.requestBTN);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            profileIV = (ImageView) itemView.findViewById(R.id.profileIV);
        }
    }
}

