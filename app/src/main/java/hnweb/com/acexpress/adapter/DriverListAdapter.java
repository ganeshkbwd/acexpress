package hnweb.com.acexpress.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.utility.ToastUlility;

/**
 * Created by neha on 2/1/2017.
 */

public class DriverListAdapter extends RecyclerView.Adapter<DriverListAdapter.DriverListViewHolder> {
    Context context;
    List<String> your_array_list;

    public DriverListAdapter(Activity activity, List<String> your_array_list) {
        this.context = activity;
        this.your_array_list = your_array_list;
    }

    @Override
    public DriverListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.driver_list_item, null);
        DriverListViewHolder dlvh = new DriverListViewHolder(layoutView);

        return dlvh;
    }

    @Override
    public void onBindViewHolder(DriverListViewHolder holder, int position) {
        holder.driverNameTV.setText(your_array_list.get(position).toString());
        if (AppConstants.MYORDERS == 1) {
            holder.requestBTN.setText("VIEW DETAILS");
            holder.requestBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ToastUlility.show(context,"Under Development...");
//                    ViewDetailsFragment vf = new ViewDetailsFragment();
//                    ((MyOrdersActivity) context).replaceFragment(vf);
                }
            });

        } else {

            holder.requestBTN.setText("REQUEST");
            holder.requestBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ToastUlility.show(context,"Under Development...");
//                    Intent intent = new Intent(context, RequestFormActivity.class);
//                    context.startActivity(intent);

                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return your_array_list.size();
    }


    public static class DriverListViewHolder extends RecyclerView.ViewHolder {
        public TextView driverNameTV;
        public Button requestBTN;

        public DriverListViewHolder(View itemView) {
            super(itemView);
            driverNameTV = (TextView) itemView.findViewById(R.id.driverNameTV);
            requestBTN = (Button) itemView.findViewById(R.id.requestBTN);
        }
    }
}
