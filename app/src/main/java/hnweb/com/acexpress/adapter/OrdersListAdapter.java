package hnweb.com.acexpress.adapter;

/**
 * Created by neha on 6/5/2017.
 */

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.driver.HomeActivity;
import hnweb.com.acexpress.fragments.driver.RequestDetailsFragment;
import hnweb.com.acexpress.models.OrdersInfo;

/**
 * Created by neha on 2/1/2017.
 */

public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListAdapter.OrederListViewHolder> {
    Activity context;
    ArrayList<OrdersInfo> your_array_list;
    String status;

    public OrdersListAdapter(Activity activity, ArrayList<OrdersInfo> your_array_list, String status) {
        this.context = activity;
        this.your_array_list = your_array_list;
        this.status = status;
    }

    @Override
    public OrederListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.request_list_item, null);
        OrederListViewHolder dlvh = new OrederListViewHolder(layoutView);

        return dlvh;
    }

    @Override
    public void onBindViewHolder(OrederListViewHolder holder, final int position) {
        holder.customerNameTV.setText(your_array_list.get(position).getName().toString());
        holder.addressTV.setText("Address : " + your_array_list.get(position).getPickupaddress());
        if (TextUtils.isEmpty(your_array_list.get(position).getProfile_photo())) {

        } else {
            Glide.with(context).load(your_array_list.get(position).getProfile_photo()).into(holder.customerIV);
        }

//        if (AppConstants.MYORDERS == 1) {
        holder.requestBTN.setText("VIEW DETAILS");
        holder.requestBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Log.e("LIST",your_array_list.get(position).toString());

                RequestDetailsFragment rdf = new RequestDetailsFragment();
                ((HomeActivity)context).replaceFragmentWithParams(rdf,your_array_list,position,status);


//                Intent intent = new Intent(context, RequestDetailsActivity.class);
//                intent.putExtra("LIST", (Serializable) your_array_list);
//                intent.putExtra("POSITION", position);
//                context.startActivity(intent);
//                    ViewDetailsFragment vf = new ViewDetailsFragment();
//                    ((MyOrdersActivity) context).replaceFragment(vf);
            }
        });
//
//        } else {
//
//            holder.requestBTN.setText("REQUEST");
//            holder.requestBTN.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(context, RequestFormActivity.class);
////                    intent.putExtra("DRIVER_ID",your_array_list.get(position).getUser_id());
//                    context.startActivity(intent);
//
//                }
//            });
//        }


    }

    @Override
    public int getItemCount() {
        return your_array_list.size();
    }


    public static class OrederListViewHolder extends RecyclerView.ViewHolder {
        public TextView customerNameTV, addressTV, pickupDateTV, pickupTimeTV;
        public Button requestBTN;
        public ImageView customerIV;

        public OrederListViewHolder(View itemView) {
            super(itemView);
            customerNameTV = (TextView) itemView.findViewById(R.id.nameTV);
            requestBTN = (Button) itemView.findViewById(R.id.viewDetailsBTN);
            addressTV = (TextView) itemView.findViewById(R.id.pickupAddressTV);
            customerIV = (ImageView) itemView.findViewById(R.id.customerIV);
            pickupDateTV = (TextView) itemView.findViewById(R.id.pickupDateTV);
            pickupTimeTV = (TextView) itemView.findViewById(R.id.pickupDateTV);
        }
    }
}

