package hnweb.com.acexpress.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import hnweb.com.acexpress.R;

/**
 * Created by neha on 2/3/2017.
 */

public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListAdapter.PaymentViewHolder> {
    Context context;
    List<String> request_list;

    public PaymentListAdapter(Activity activity, List<String> request_list) {
        this.context = activity;
        this.request_list = request_list;
    }

    @Override
    public PaymentListAdapter.PaymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.payment_list_item, null);
        PaymentListAdapter.PaymentViewHolder rla = new PaymentListAdapter.PaymentViewHolder(layoutView);
        return rla;
    }

    @Override
    public void onBindViewHolder(PaymentListAdapter.PaymentViewHolder holder, int position) {
        holder.nameTV.setText(request_list.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return request_list.size();
    }

    public class PaymentViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTV;

        public PaymentViewHolder(View itemView) {
            super(itemView);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);


        }
    }
}
