package hnweb.com.acexpress.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.driver.RequestDetailsActivity;
import hnweb.com.acexpress.utility.DrawerClick;

/**
 * Created by neha on 2/3/2017.
 */

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.RequestViewHolder> {
    Context context;
    List<String> request_list;

    public RequestListAdapter(Activity activity, List<String> request_list) {
        this.context = activity;
        this.request_list = request_list;
    }

    @Override
    public RequestListAdapter.RequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.request_list_item, null);
        RequestListAdapter.RequestViewHolder rla = new RequestListAdapter.RequestViewHolder(layoutView);
        return rla;
    }

    @Override
    public void onBindViewHolder(RequestListAdapter.RequestViewHolder holder, int position) {
        holder.nameTV.setText(request_list.get(position).toString());


        holder.viewDetailsBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DrawerClick.intentCall((Activity) context, RequestDetailsActivity.class);
//                ToastUlility.show(context, "Coming Soon...");
            }
        });
    }

    @Override
    public int getItemCount() {
        return request_list.size();
    }

    public class RequestViewHolder extends RecyclerView.ViewHolder {
        public Button viewDetailsBTN;
        public TextView nameTV;

        public RequestViewHolder(View itemView) {
            super(itemView);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            viewDetailsBTN = (Button) itemView.findViewById(R.id.viewDetailsBTN);

        }
    }
}
