package hnweb.com.acexpress.application;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by neha on 2/2/2017.
 */

public class AppConstants {
    public static int MYORDERS = 0;
    public static int ORDER = 1;
    public static boolean ISDRIVER = false;
    public static String NEWREQUEST = "";
    public static boolean ISPAID = false;



    public static String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }
}
