package hnweb.com.acexpress.application;

/**
 * Created by neha on 2/16/2017.
 */

public class AppParams {

    //register
    public static String EMAIL_ID = "email_address";
    public static String NAME = "name";
    public static String PHONE = "phone";
    public static String ADDRESS = "address";
    public static String USER_TYPE = "user_type";
    public static String PASSWORD = "password";

    //verify
    public static String USER_ID = "user_id";
    public static String VERIFICATION_CODE = "verification_code";

    //changepass
    public static String OLDPASS = "current_password";
    public static String NEWPASS = "new_password";
    public static String OTP = "otp";

    //edit profile
    public static String PROFILE_PHOTO = "profile_photo";

    //request form

    public static String PICKUP_DATE = "pickupdate";
    public static String PICKUP_TIME = "picktime";
    public static String PICKUP_ADDRESS = "pickupaddress";
    public static String DELIVERY_ADDRESS = "deliveryaddress";
    public static String WEIGHT = "weight";
    public static String DIMENSION = "dimension";
    public static String DISTANCE = "distance";
    public static String DURATION = "duration";
    public static String DRIVER_ID = "userid_driver";
    public static String CUSTOMER_ID = "userid_customer";
    public static String VEHICLE_TYPE = "type_of_vehicle";
    public static String DELIVERY_TIME = "delivery_time";

    // Send Request
    public static String ESTIMATE_ID = "estimate_id";


    public static String REQUEST_STATUS = "request_status"; //statuses : ACCEPTED ,  DECLINED , PENDING


}
