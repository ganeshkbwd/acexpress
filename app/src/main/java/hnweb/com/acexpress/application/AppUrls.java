package hnweb.com.acexpress.application;

/**
 * Created by neha on 2/16/2017.
 */

public class AppUrls {

    public static String TCP_SERVER_ADDRESS = "162.220.163.185";
    //    public static int TCP_SERVER_PORT = 8802;
    public static int TCP_SERVER_PORT = 4025;

    public static final String TCP_ACTION_CLIENT_CONNECTED = "client_connected";
    public static final String TCP_ACTION_ACTION_USER_LOGIN = "login_user";
    public static final String TCP_ACTION_DRIVER_REQUEST_REPLY = "driver_request_reply";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_TYPE = "user_type";
    public static final String DRIVER_TRACKING_ENABLED = "is_driver_tracking_enabled";
    public static final String TCP_ACTION_UPDATE_DRIVER_TRACKING = "update_tracking_with_broadcast";








    public static String BASE_URL = "http://162.220.163.185/~acexpresss/webservice/";

    public static String REGISTER_URL = BASE_URL + "register.php";
    public static String VERIFY_URL = BASE_URL + "account_activation.php";
    public static String LOGIN_URL = BASE_URL + "login.php";
    public static String FORGOT_URL = BASE_URL + "forgotpassword.php";
    public static String CHANGEPASS_URL = BASE_URL + "change_password.php";
    public static String MYPROFILE_URL = BASE_URL + "get_profile.php";
    public static String EDITPROFILE_URL = BASE_URL + "update_profile.php";

    public static String GETALLDRIVERS = BASE_URL + "get_drivers_list.php";
    public static String GET_ESTIMATES = BASE_URL + "get_estimate.php";

    public static String GOOGLE_DISTANCE_API_KEY = "AIzaSyBv_dwt_W1Sr0EQQepZQJlrTIUFWbrmSP0";
    public static String SEND_REQUEST = BASE_URL + "send_request.php";


    //driver
    public static String NEWORDERS = BASE_URL + "new_and_accepted_request.php";
    public static String ACCEPDECLINE = BASE_URL + "accept_decline_request_by_driver.php";

    //otp password
    public static String OTP_PASSWORD = BASE_URL + "change_password_before_login.php";

    public static String CUSTOMER_MYREQUEST = BASE_URL + "api_customer_request.php";
    public static String START_COMPLETE_SERVICE = BASE_URL + "accept_decline_request_by_driver.php";


}
