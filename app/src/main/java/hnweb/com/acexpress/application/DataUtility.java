package hnweb.com.acexpress.application;

/**
 * Created by neha on 2/20/2017.
 */


import android.content.Context;

import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.models.RequestInfo;

/**
 * Created by shree on 06-12-2016.
 */
public class DataUtility {


    public static void submitRequest(Context context, final RequestInfo request_info, final OnDataCallbackListner listner) {
//        final CacheUtility cacheUtility = new CacheUtility(context);

//        final String request_tag = request_info.getRequestTag()== null ? request_info.getUrl():request_info.getRequestTag();
        String url = request_info.getUrl();

        StringBuffer sb = new StringBuffer(url);
        if (request_info.getMethod() == RequestInfo.METHOD_GET) {
            Map<String, String> params = request_info.getParams();
            if (params != null) {
                if (params.size() > 0) {
                    sb.append("?");
                }
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    sb.append(key + "=" + value + "&");
                }
                url = sb.toString();
            }
        }
        final StringRequest stringRequest = new StringRequest(request_info.getMethod(), url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (listner != null) {
                            listner.OnDataReceived(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (isNetworkError(error))
                    listner.OnError(error.getLocalizedMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (request_info.getMethod() == RequestInfo.METHOD_POST) {
                    if (request_info.getParams() != null) {
                        return request_info.getParams();
                    }
                }
                return params;


            }

        };

        MainApplication.getInstance().addToRequestQueue(stringRequest, "request_tag");
    }


    public interface OnDataCallbackListner {
        public void OnDataReceived(String data);

        public void OnError(String message);
    }

    private static boolean isNetworkError(VolleyError volleyError) {
        boolean have_error = false;

        if (volleyError instanceof NetworkError) {
            have_error = true;
        } else if (volleyError instanceof ServerError) {

        } else if (volleyError instanceof AuthFailureError) {

        } else if (volleyError instanceof ParseError) {

        } else if (volleyError instanceof NoConnectionError) {
            have_error = true;
        } else if (volleyError instanceof TimeoutError) {

        }
        return have_error;
    }

}
