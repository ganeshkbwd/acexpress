package hnweb.com.acexpress.drawPolyLine;

import com.google.android.gms.maps.GoogleMap;

/**
 * Created by neha on 7/7/2017.
 */

public class ParserTaskParams {

    GoogleMap mMap;
    String result1;
    String[] result;

    public ParserTaskParams(GoogleMap mMap, String result2) {
        this.mMap = mMap;
        this.result1 = result2;
        result = new String[]{result1};
    }
}
