package hnweb.com.acexpress.drawPolyLine;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;

/**
 * Created by neha on 7/7/2017.
 */

public class ReadTask extends AsyncTask<String, Void, String> {

    String url;
    GoogleMap mMap;
    Activity activity;

    public ReadTask(String url, GoogleMap mMap, Activity activity) {
        this.url = url;
        this.mMap = mMap;
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... url) {
        // TODO Auto-generated method stub
        String data = "";
        try {
            MapHttpConnection http = new MapHttpConnection();
            data = http.readUr(url[0]);


        } catch (Exception e) {
            // TODO: handle exception
            Log.d("Background Task", e.toString());
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//        new ParserTask(mMap,result).execute();
    }

}
