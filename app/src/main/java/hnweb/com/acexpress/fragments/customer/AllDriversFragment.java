package hnweb.com.acexpress.fragments.customer;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.customer.FindDriversActivity;
import hnweb.com.acexpress.adapter.AllDriversListAdapter;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.models.DriversInfo;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;

/**
 * Created by neha on 1/25/2017.
 */

public class AllDriversFragment extends Fragment {

    private LoadingDialog loadingDialog;
    ArrayList<DriversInfo> driversInfoArrayList = new ArrayList<DriversInfo>();
    RecyclerView lv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.alldrivers_fragment, container, false);
        Toast.makeText(getActivity(), "ALLDRIVRES CLICKED", Toast.LENGTH_SHORT).show();
        loadingDialog = new LoadingDialog(getActivity());
        lv = (RecyclerView) myFragmentView.findViewById(R.id.allDriverRV);
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));
        ((FindDriversActivity) getActivity()).searchIV.setVisibility(View.GONE);
        getAllDrivers();

        return myFragmentView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void getAllDrivers() {
        loadingDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.GETALLDRIVERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
//                ToastUlility.show(All.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
//                    String message = jobj.getString("message");
                    if (msg_code == 1) {
                        JSONArray jarr = jobj.getJSONArray("response");
                        for (int i = 0; i < jarr.length(); i++) {
                            DriversInfo driversInfo = new DriversInfo();
                            driversInfo.setUser_id(jarr.getJSONObject(i).getString("user_id"));
                            driversInfo.setName(jarr.getJSONObject(i).getString("name"));
                            driversInfo.setPhone(jarr.getJSONObject(i).getString("phone"));
                            driversInfo.setAddress(jarr.getJSONObject(i).getString("address"));
                            driversInfo.setUser_type(jarr.getJSONObject(i).getString("user_type"));
                            driversInfo.setEmail_address(jarr.getJSONObject(i).getString("email_address"));
                            driversInfo.setProfile_photo(jarr.getJSONObject(i).getString("profile_photo"));
                            driversInfo.setStatus(jarr.getJSONObject(i).getString("status"));

                            driversInfoArrayList.add(driversInfo);
                        }

                        AllDriversListAdapter dla = new AllDriversListAdapter(getActivity(), driversInfoArrayList);
                        lv.setAdapter(dla);


                    } else {
                        String message = jobj.getString("message");
                        ToastUlility.show(getActivity(), message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(getActivity(), "Network Error,please try again");
            }

        });

        // Add the request to the RequestQueue.
        String request_tag = "all_drivers";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }
}
