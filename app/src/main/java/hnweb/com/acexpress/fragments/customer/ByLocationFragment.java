package hnweb.com.acexpress.fragments.customer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.Arrays;
import java.util.List;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.customer.FindDriversActivity;
import hnweb.com.acexpress.adapter.DriverListAdapter;
import hnweb.com.acexpress.utility.GPS;
import hnweb.com.acexpress.utility.GeocodingLocation;
import hnweb.com.acexpress.utility.ToastUlility;

/**
 * Created by neha on 1/25/2017.
 */

public class ByLocationFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "DemoActivity";
    private SlidingUpPanelLayout mLayout;
    MapView mMapView;
    GoogleMap googleMap;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    //To store longitude and latitude from map
    private double longitude;
    private double latitude;


    public ByLocationFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.drivers_by_location, container, false);
        slidingPanel(myFragmentView);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mMapView = ((MapView) myFragmentView.findViewById(R.id.mapview));
        mMapView.onCreate(savedInstanceState);

        ((FindDriversActivity) getActivity()).searchIV.setVisibility(View.VISIBLE);
        ((FindDriversActivity) getActivity()).searchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((FindDriversActivity) getActivity()).searchET.getVisibility() == View.VISIBLE) {
                    if (((FindDriversActivity) getActivity()).searchET.getText().toString().trim().length() > 0) {
                        GeocodingLocation locationAddress = new GeocodingLocation();
                        locationAddress.getAddressFromLocation(((FindDriversActivity) getActivity()).searchET.getText().toString().trim(),
                                getActivity().getApplicationContext(), new GeocoderHandler());
                    } else {
                        ((FindDriversActivity) getActivity()).byLocationTV.setVisibility(View.VISIBLE);
                        ((FindDriversActivity) getActivity()).allDriversTV.setVisibility(View.VISIBLE);
                        ((FindDriversActivity) getActivity()).byTV.setVisibility(View.VISIBLE);
                        ((FindDriversActivity) getActivity()).allTV.setVisibility(View.INVISIBLE);
                        ((FindDriversActivity) getActivity()).searchET.setVisibility(View.GONE);
                    }

                } else {
                    ((FindDriversActivity) getActivity()).byLocationTV.setVisibility(View.GONE);
                    ((FindDriversActivity) getActivity()).allDriversTV.setVisibility(View.GONE);
                    ((FindDriversActivity) getActivity()).byTV.setVisibility(View.VISIBLE);
                    ((FindDriversActivity) getActivity()).allTV.setVisibility(View.INVISIBLE);
                    ((FindDriversActivity) getActivity()).searchET.setVisibility(View.VISIBLE);
                }
            }
        });

        GPS.checkGPS(getActivity());
        setMap(myFragmentView);
        listViwe(myFragmentView);

        return myFragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setMap(View rootView) {
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);
            }
        });


    }

    public void slidingPanel(View v) {
        mLayout = (SlidingUpPanelLayout) v.findViewById(R.id.sliding_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.i(TAG, "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

//        mLayout.setAnchorPoint(0.7f);
//        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
    }


    public void listViwe(View v) {
        RecyclerView lv = (RecyclerView) v.findViewById(R.id.list);
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));


        List<String> your_array_list = Arrays.asList(
                "This",
                "Is",
                "An",
                "Example",
                "ListView",
                "That",
                "You",
                "Can",
                "Scroll",
                ".",
                "It",
                "Shows",
                "How",
                "Any",
                "Scrollable",
                "View",
                "Can",
                "Be",
                "Included",
                "As",
                "A",
                "Child",
                "Of",
                "SlidingUpPanelLayout"
        );


        DriverListAdapter dla = new DriverListAdapter(getActivity(), your_array_list);
        lv.setAdapter(dla);

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        getCurrentLocation();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //Function to move the map
    private void moveMap() {
        //String to display current latitude and longitude
        String msg = latitude + ", " + longitude;

        //Creating a LatLng Object to store Coordinates
        LatLng latLng = new LatLng(latitude, longitude);

        //Adding marker to map
        googleMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
                .title("Current Location")); //Adding a title

        //Moving the camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        //Animating the camera
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        //Displaying current coordinates in toast
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    //Getting current location
    private void getCurrentLocation() {
        googleMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            //moving the map to location
            moveMap();
        }
    }


    public class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }

            if (TextUtils.isEmpty(locationAddress)) {

                ToastUlility.show(getActivity(),"Address not found, please enter valid address.");
            } else {
                String[] latLong = locationAddress.split(",");
                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(latLong[0]),
                                Double.parseDouble(latLong[1])));
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(07);

//            ByLocationFragment bf = new ByLocationFragment();
//            bf.getLatLoing(latLong);
//            googleMap.moveCamera(center);
//            googleMap.animateCamera(zoom);
                googleMap.clear();
                latitude = Double.parseDouble(latLong[0]);
                longitude = Double.parseDouble(latLong[1]);
                moveMap();
            }

//            getAllTherapist(Double.parseDouble(latLong[0]), Double.parseDouble(latLong[1]));

        }
    }
}
