package hnweb.com.acexpress.fragments.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hnweb.com.acexpress.R;

/**
 * Created by neha on 2/2/2017.
 */

public class PaymentFragment extends Fragment {
    public PaymentFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.payment_layout, container, false);
        return myFragmentView;
    }
}
