package hnweb.com.acexpress.fragments.customer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.adapter.CustomerRequestsAdapter;
import hnweb.com.acexpress.adapter.DriverListAdapter;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.models.CustomerRequestInfo;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;

/**
 * Created by neha on 2/2/2017.
 */

public class PendingRequestFragment extends Fragment {

    LoadingDialog loadingDialog;
    SharedPreferences sharedPreferences;
    ArrayList<CustomerRequestInfo> customerRequestInfoArrayList = new ArrayList<CustomerRequestInfo>();
    RecyclerView lv;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.alldrivers_fragment, container, false);
        loadingDialog = new LoadingDialog(getActivity());
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        lv = (RecyclerView) myFragmentView.findViewById(R.id.allDriverRV);
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));
//        listViwe(myFragmentView);
        getAllPendingRequests();
        return myFragmentView;
    }

    public void listViwe(View v) {
        RecyclerView lv = (RecyclerView) v.findViewById(R.id.allDriverRV);
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));


        List<String> your_array_list = Arrays.asList(
                "This",
                "Is",
                "An",
                "Example",
                "ListView",
                "That",
                "You",
                "Can",
                "Scroll",
                ".",
                "It",
                "Shows",
                "How",
                "Any",
                "Scrollable",
                "View",
                "Can",
                "Be",
                "Included",
                "As",
                "A",
                "Child",
                "Of",
                "SlidingUpPanelLayout"
        );


        DriverListAdapter dla = new DriverListAdapter(getActivity(), your_array_list);
        lv.setAdapter(dla);

    }

    public void getAllPendingRequests() {
        loadingDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.CUSTOMER_MYREQUEST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
//                ToastUlility.show(All.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
//                    String message = jobj.getString("message");
                    if (msg_code == 1) {
                        JSONArray jarr = jobj.getJSONArray("response");
                        for (int i = 0; i < jarr.length(); i++) {
                            CustomerRequestInfo driversInfo = new CustomerRequestInfo();
                            driversInfo.setEstimate_id(jarr.getJSONObject(i).getString("estimate_id"));
                            driversInfo.setName(jarr.getJSONObject(i).getString("name"));
                            driversInfo.setPhone(jarr.getJSONObject(i).getString("phone"));
                            driversInfo.setAddress(jarr.getJSONObject(i).getString("address"));
                            driversInfo.setEmail_address(jarr.getJSONObject(i).getString("email_address"));
                            driversInfo.setProfile_photo(jarr.getJSONObject(i).getString("profile_photo"));
                            driversInfo.setPickupdate(jarr.getJSONObject(i).getString("pickupdate"));
                            driversInfo.setPicktime(jarr.getJSONObject(i).getString("picktime"));
                            driversInfo.setPickupaddress(jarr.getJSONObject(i).getString("pickupaddress"));
                            driversInfo.setDeliveryaddress(jarr.getJSONObject(i).getString("deliveryaddress"));
                            driversInfo.setWeight(jarr.getJSONObject(i).getString("weight"));
                            driversInfo.setDimension(jarr.getJSONObject(i).getString("dimension"));
                            driversInfo.setDistance(jarr.getJSONObject(i).getString("distance"));
                            driversInfo.setDuration(jarr.getJSONObject(i).getString("duration"));
                            driversInfo.setUserid_customer(jarr.getJSONObject(i).getString("userid_customer"));
                            driversInfo.setUserid_driver(jarr.getJSONObject(i).getString("userid_driver"));
                            driversInfo.setType_of_vehicle(jarr.getJSONObject(i).getString("type_of_vehicle"));
                            driversInfo.setDelivery_time(jarr.getJSONObject(i).getString("delivery_time"));
                            driversInfo.setRequest_status(jarr.getJSONObject(i).getString("request_status"));
                            driversInfo.setMileage_cost(jarr.getJSONObject(i).getString("mileage_cost"));
                            driversInfo.setFuel_charges(jarr.getJSONObject(i).getString("fuel_charges"));
                            driversInfo.setTotal_charges(jarr.getJSONObject(i).getString("total_charges"));

                            customerRequestInfoArrayList.add(driversInfo);
                        }

                        CustomerRequestsAdapter dla = new CustomerRequestsAdapter(getActivity(), customerRequestInfoArrayList, "PENDING");
                        lv.setAdapter(dla);


                    } else {
                        String message = jobj.getString("message");
                        ToastUlility.show(getActivity(), message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(getActivity(), "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.CUSTOMER_ID, String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
                params.put(AppParams.REQUEST_STATUS, "PENDING");

                Log.e("PARAMS", params.toString());
                return params;

            }
        };

        // Add the request to the RequestQueue.
        String request_tag = "all_drivers";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }
}
