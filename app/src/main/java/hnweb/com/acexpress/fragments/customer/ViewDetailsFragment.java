package hnweb.com.acexpress.fragments.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.customer.MyOrdersActivity;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.models.CustomerRequestInfo;

/**
 * Created by neha on 2/2/2017.
 */

public class ViewDetailsFragment extends Fragment {
    Button trackPayBTN;
    TextView totalCostTV, fuelChargeTV, serviceCostTV, distanceDurationTV, milageCostTV,
            weightTV, dimensionsTV, deliveryAddressTV, pickupAddressTV, pickupTimeTV, pickupDateTV;
    Bundle bundle;
    ArrayList<CustomerRequestInfo> customerRequestInfoArrayList = new ArrayList<CustomerRequestInfo>();
    int position;
    String status;

    public ViewDetailsFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.view_details_fragment, container, false);

        trackPayBTN = (Button) myFragmentView.findViewById(R.id.trackPayBTN);


        init(myFragmentView);
        try {
            bundle = getArguments();
            position = bundle.getInt("POSITION");
            customerRequestInfoArrayList = (ArrayList<CustomerRequestInfo>) bundle.getSerializable("LIST");
            status = bundle.getString("STATUS");
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if(customerRequestInfoArrayList.size()!=0){
        setData();
//        }


        if (AppConstants.ORDER == 1) {
            trackPayBTN.setVisibility(View.GONE);
        } else if (AppConstants.ORDER == 2) {
            trackPayBTN.setVisibility(View.VISIBLE);
            trackPayBTN.setText("VIEW TRACK");
            trackPayBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OngoingViewTrackRequest vf = new OngoingViewTrackRequest();
                    ((MyOrdersActivity) getActivity()).replaceFragment(vf);
                }
            });
        } else if (AppConstants.ORDER == 3) {
            trackPayBTN.setVisibility(View.VISIBLE);
            trackPayBTN.setText("MAKE A PAYMENT");
            trackPayBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PaymentFragment vf = new PaymentFragment();
                    ((MyOrdersActivity) getActivity()).replaceFragment(vf);
                }
            });
        }

        return myFragmentView;
    }

    private void setData() {
        pickupDateTV.setText(customerRequestInfoArrayList.get(position).getPickupdate());
        pickupTimeTV.setText(customerRequestInfoArrayList.get(position).getPicktime());
        pickupAddressTV.setText(customerRequestInfoArrayList.get(position).getPickupaddress());
        deliveryAddressTV.setText(customerRequestInfoArrayList.get(position).getDeliveryaddress());
        dimensionsTV.setText(customerRequestInfoArrayList.get(position).getDimension());
        weightTV.setText(customerRequestInfoArrayList.get(position).getWeight());
        milageCostTV.setText(customerRequestInfoArrayList.get(position).getMileage_cost());
        distanceDurationTV.setText(customerRequestInfoArrayList.get(position).getDuration());
//        serviceCostTV.setText(customerRequestInfoArrayList.get(position).getC);
        fuelChargeTV.setText(customerRequestInfoArrayList.get(position).getFuel_charges());
        totalCostTV.setText(customerRequestInfoArrayList.get(position).getTotal_charges());


    }

    public void init(View v) {
        pickupDateTV = (TextView) v.findViewById(R.id.pickupDateTV);
        pickupTimeTV = (TextView) v.findViewById(R.id.pickupTimeTV);
        pickupAddressTV = (TextView) v.findViewById(R.id.pickupAddressTV);
        deliveryAddressTV = (TextView) v.findViewById(R.id.deliveryAddressTV);
        dimensionsTV = (TextView) v.findViewById(R.id.dimensionsTV);
        weightTV = (TextView) v.findViewById(R.id.weightTV);
        milageCostTV = (TextView) v.findViewById(R.id.milageCostTV);
        distanceDurationTV = (TextView) v.findViewById(R.id.distanceDurationTV);
        serviceCostTV = (TextView) v.findViewById(R.id.serviceCostTV);
        fuelChargeTV = (TextView) v.findViewById(R.id.fuelChargeTV);
        totalCostTV = (TextView) v.findViewById(R.id.totalCostTV);
    }
}
