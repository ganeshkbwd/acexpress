package hnweb.com.acexpress.fragments.driver;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.driver.HomeActivity;
import hnweb.com.acexpress.adapter.OrdersListAdapter;
import hnweb.com.acexpress.adapter.RequestListAdapter;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.models.OrdersInfo;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;

/**
 * Created by neha on 2/3/2017.
 */

public class DriverPendingRequestFragment extends Fragment {

    private LoadingDialog loadingDialog;
    SharedPreferences sharedPreferences;
    ArrayList<OrdersInfo> ordersInfoArrayList = new ArrayList<OrdersInfo>();
    RecyclerView lv;

    public DriverPendingRequestFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.request_list_fragment, container, false);
        ((HomeActivity) getActivity()).titleLL.setVisibility(View.VISIBLE);
        sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), 0);
        lv = (RecyclerView) myFragmentView.findViewById(R.id.requestRV);
        loadingDialog = new LoadingDialog(getActivity());
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));
        getPendingOrders(sharedPreferences.getInt("USER_ID", 0));
//        listViwe(myFragmentView);
        return myFragmentView;
    }

    public void listViwe(View v) {
        RecyclerView lv = (RecyclerView) v.findViewById(R.id.requestRV);
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));


        List<String> your_array_list = Arrays.asList(
                "John Bravo",
                "Mike Taylor",
                "Arron Finch",
                "Ricky Gibs",
                "John Snow"
        );


        RequestListAdapter dla = new RequestListAdapter(getActivity(), your_array_list);
        lv.setAdapter(dla);

    }

    public void getPendingOrders(final int user_id) {
        loadingDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.NEWORDERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
//                ToastUlility.show(All.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
//                    String message = jobj.getString("message");
                    if (msg_code == 1) {
                        ordersInfoArrayList.clear();
                        JSONArray jarr = jobj.getJSONArray("response");
                        for (int i = 0; i < jarr.length(); i++) {
                            OrdersInfo driversInfo = new OrdersInfo();
                            driversInfo.setEstimate_id(jarr.getJSONObject(i).getString("estimate_id"));
                            driversInfo.setPickupdate(jarr.getJSONObject(i).getString("pickupdate"));
                            driversInfo.setPicktime(jarr.getJSONObject(i).getString("picktime"));
                            driversInfo.setPickupaddress(jarr.getJSONObject(i).getString("pickupaddress"));
                            driversInfo.setDeliveryaddress(jarr.getJSONObject(i).getString("deliveryaddress"));
                            driversInfo.setDelivery_time(jarr.getJSONObject(i).getString("delivery_time"));
                            driversInfo.setProfile_photo(jarr.getJSONObject(i).getString("profile_photo"));
                            driversInfo.setName(jarr.getJSONObject(i).getString("name"));
                            driversInfo.setPhone(jarr.getJSONObject(i).getString("phone"));
                            driversInfo.setAddress(jarr.getJSONObject(i).getString("address"));
                            driversInfo.setEmail_address(jarr.getJSONObject(i).getString("email_address"));
                            driversInfo.setDimension(jarr.getJSONObject(i).getString("dimension"));
                            driversInfo.setWeight(jarr.getJSONObject(i).getString("weight"));

                            ordersInfoArrayList.add(driversInfo);
                        }
//
                        OrdersListAdapter dla = new OrdersListAdapter(getActivity(), ordersInfoArrayList, "PENDING");
                        lv.setAdapter(dla);


                    } else {
                        String message = jobj.getString("message");
                        ToastUlility.show(getActivity(), message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(getActivity(), "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.DRIVER_ID, String.valueOf(user_id));
                params.put(AppParams.REQUEST_STATUS, "ACCEPTED");

                Log.e("PARAMS", params.toString());
                return params;

            }
        };


        // Add the request to the RequestQueue.
        String request_tag = "new_orders";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }
}
