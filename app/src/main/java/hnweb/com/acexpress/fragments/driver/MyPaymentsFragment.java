package hnweb.com.acexpress.fragments.driver;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hnweb.com.acexpress.R;

/**
 * Created by neha on 7/6/2017.
 */

public class MyPaymentsFragment extends Fragment {

    TextView prqTV, nrqTV, myPaymentTV;
    SharedPreferences sharedPreferences;


    public MyPaymentsFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_my_payments, container, false);

        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);


        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
