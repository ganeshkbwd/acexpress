package hnweb.com.acexpress.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.adapter.PaymentListAdapter;
import hnweb.com.acexpress.adapter.RequestListAdapter;

/**
 * Created by neha on 2/3/2017.
 */

public class PendingPaymentFragment extends Fragment {
    public PendingPaymentFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.request_list_fragment, container, false);
        listViwe(myFragmentView);
        return myFragmentView;
    }

    public void listViwe(View v) {
        RecyclerView lv = (RecyclerView) v.findViewById(R.id.requestRV);
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));


        List<String> your_array_list = Arrays.asList(
                "John Bravo",
                "Mike Taylor",
                "Arron Finch",
                "Ricky Gibs",
                "John Snow"
        );


        PaymentListAdapter dla = new PaymentListAdapter(getActivity(), your_array_list);
        lv.setAdapter(dla);

    }
}
