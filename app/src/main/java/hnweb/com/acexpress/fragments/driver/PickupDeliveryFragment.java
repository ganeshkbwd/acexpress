package hnweb.com.acexpress.fragments.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.driver.HomeActivity;
import hnweb.com.acexpress.activity.driver.MyPaymentActivity;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.models.OrdersInfo;
import hnweb.com.acexpress.utility.GPS;
import hnweb.com.acexpress.utility.GetLatLongFromAddress;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;

/**
 * Created by neha on 7/6/2017.
 */

public class PickupDeliveryFragment extends Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, HomeActivity.OnLocationChangelistner, OnMapReadyCallback {

    TextView myPaymentTV;
    //    SupportMapFragment mapFragment;
//    MapView mMapView;
    private GoogleMap mMap;
    private Marker positionMarker;
    private Circle circleMarker;
    Button serviceBTN;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    SharedPreferences sharedPreferences;
    private LoadingDialog loadingDialog;
    int ESTIMATE_ID, listPosition;
    String status;
    ArrayList<OrdersInfo> ordersInfoArrayList;
//    GoogleMap googleMap;

    public PickupDeliveryFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_pickup_delivery, container, false);

        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        loadingDialog = new LoadingDialog(getActivity());
        listPosition = getArguments().getInt("POSITION");
        status = getArguments().getString("STATUS");

        ordersInfoArrayList = (ArrayList<OrdersInfo>) getArguments().getSerializable("LIST");
        ESTIMATE_ID = Integer.parseInt(ordersInfoArrayList.get(listPosition).getEstimate_id());
        ((HomeActivity) getActivity()).titleLL.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).toolbar.setTitle("PICKUP DELIVERY");
//        setToolbarDrawer();

//        mMapView = (MapView) myFragmentView.findViewById(R.id.map);
//        mMapView.onCreate(savedInstanceState);
        serviceBTN = (Button) myFragmentView.findViewById(R.id.serviceBTN);
        serviceBTN.setOnClickListener(this);
        SupportMapFragment mapFragment = new SupportMapFragment();
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.map_frame, mapFragment).commit();
        mapFragment.getMapAsync(this);
        GPS.checkGPS(getActivity());

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }
//        HomeActivity homeActivity = new HomeActivity();
        ((HomeActivity) getActivity()).setLocationChangedlistner(this);

        if (status.equalsIgnoreCase("ONGOING")) {
            serviceBTN.setText("Service completed");
        } else if (status.equalsIgnoreCase("PENDING")) {
            serviceBTN.setText("Start service");
        } else if (status.equalsIgnoreCase("COMPLETED")) {
            serviceBTN.setText("Service Already Completed");
        }
//        drawPath();
        return myFragmentView;
    }

//    public void setMap(View v) {
//
//
//
////        mMapView.onResume(); // needed to get the map to display immediately
////
////        try {
////            MapsInitializer.initialize(getActivity().getApplicationContext());
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        mMapView.getMapAsync(new OnMapReadyCallback() {
////            @Override
////            public void onMapReady(GoogleMap mMap) {
////                googleMap = mMap;
////
////                // For showing a move to my location button
////                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
////                    // TODO: Consider calling
////                    //    ActivityCompat#requestPermissions
////                    // here to request the missing permissions, and then overriding
////                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
////                    //                                          int[] grantResults)
////                    // to handle the case where the user grants the permission. See the documentation
////                    // for ActivityCompat#requestPermissions for more details.
////                    return;
////                }
////
////                mMap.setMyLocationEnabled(true);
////                LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
////                Criteria criteria = new Criteria();
////                String provider = locationManager.getBestProvider(criteria, true);
////                Location location = locationManager.getLastKnownLocation(provider);
////
////
////                if (location != null) {
////                    double latitude = location.getLatitude();
////                    double longitude = location.getLongitude();
////                    LatLng latLng = new LatLng(latitude, longitude);
////                    LatLng myPosition = new LatLng(latitude, longitude);
////
////
////                    LatLng coordinate = new LatLng(latitude, longitude);
////                    CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 19);
////                    mMap.animateCamera(yourLocation);
////                }
//
//
////        mapFragment = ((SupportMapFragment) getActivity().getSupportFragmentManager()
////                .findFragmentById(R.id.map));
////        mapFragment.getMapAsync(this);
//
//
//            }


//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//        LatLng myPosition;
//        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED &&
//                ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
//                        != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        mMap.setMyLocationEnabled(true);
//        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        String provider = locationManager.getBestProvider(criteria, true);
//        Location location = locationManager.getLastKnownLocation(provider);
//
//
//        if (location != null) {
//            double latitude = location.getLatitude();
//            double longitude = location.getLongitude();
//            LatLng latLng = new LatLng(latitude, longitude);
//            myPosition = new LatLng(latitude, longitude);
//
//
//            LatLng coordinate = new LatLng(latitude, longitude);
//            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 19);
//            mMap.animateCamera(yourLocation);
//        }
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.serviceBTN:
                if (serviceBTN.getText().toString().trim().equalsIgnoreCase("start service")) {
                    serviceBTN.setText("Service completed");
                    doServiceActions(String.valueOf(ESTIMATE_ID), "ONGOING");
                    ToastUlility.show(getActivity(), "Service Started Successfully");
                } else {
                    serviceBTN.setText("Start service");
                    doServiceActions(String.valueOf(ESTIMATE_ID), "COMPLETED");
                    ToastUlility.show(getActivity(), "Service Completed Successfully");
                }

                break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        if (mLastLocation != null && mMap != null) {

            LatLng position = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16.0f));
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

//    @Override
//    public void onResume() {
//        super.onResume();
////        setUpMap();
//        mMapView.onResume();
//
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        ////        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
//        mMapView.onPause();
//
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mMapView.onDestroy();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mMapView.onLowMemory();
//    }

    public void doServiceActions(final String user_id, final String status) {
        loadingDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.START_COMPLETE_SERVICE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
//                ToastUlility.show(All.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
//                    String message = jobj.getString("message");
                    if (msg_code == 1) {

                        if (status.equals("COMPLETED")) {
                            Intent intent = new Intent(getActivity(), MyPaymentActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }


                    } else {
                        String message = jobj.getString("message");
                        ToastUlility.show(getActivity(), message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(getActivity(), "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.ESTIMATE_ID, user_id);
                params.put(AppParams.REQUEST_STATUS, status);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };


        // Add the request to the RequestQueue.
        String request_tag = "new_orders";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }


    @Override
    public void OnLocationChanged(Location location) {
        updateMarker(location);
    }

    private void updateMarker(Location location) {
        //  Log.i("TrackView","update Marker called"+location.toString());

        if (mMap != null) {
            if (positionMarker == null) {
                MarkerOptions positionMarkerOptions = new MarkerOptions();
                positionMarkerOptions.draggable(false);
                positionMarkerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_action_pick_map);
                positionMarkerOptions.icon(icon);
                positionMarkerOptions.flat(true);
                positionMarkerOptions.anchor(0.5f, 0.5f);
                positionMarker = mMap.addMarker(positionMarkerOptions);
                Log.i("GPS", "bearing angle= " + location.getLatitude() + "\n" + location.getLongitude());

                CircleOptions circleMarkerOptions = new CircleOptions();
                circleMarkerOptions.center(new LatLng(location.getLatitude(), location.getLongitude()));
                circleMarkerOptions.radius(location.getAccuracy());
                circleMarkerOptions.fillColor(Color.parseColor("#40E52F2F"));
                circleMarkerOptions.strokeWidth(1.0f);
                circleMarkerOptions.strokeColor(Color.parseColor("#E52F2F"));
                circleMarker = mMap.addCircle(circleMarkerOptions);
            } else {
                Log.i("GPS", "bearing angle= " + location.getBearing());
                positionMarker.setRotation(location.getBearing());
                positionMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
                circleMarker.setRadius(location.getAccuracy());
                circleMarker.setCenter(new LatLng(location.getLatitude(), location.getLongitude()));
            }

        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.mMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        googleMap.setTrafficEnabled(false);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Location mLastLocation = ((HomeActivity) getActivity()).getLastLocation();
                if (mLastLocation != null) {
                    updateMarker(mLastLocation);
                    LatLng position = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16.0f));
                    drawPath();


//                    if (request_info != null) {
//                        cname_textView.setText(request_info.getCustomerName());
//                    if ()
//                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(GetLatLongFromAddress.get(ordersInfoArrayList.get(listPosition).getPickupaddress(), getActivity()), 16.0f));
//                        getRouteInfoData(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new LatLng(request_info.getSourceLatitude(), request_info.getSourceLongitude()));
//                    }


                }
            }
        }, 200);

    }


    public void drawPath() {

        LatLng startLatLong = GetLatLongFromAddress.get(ordersInfoArrayList.get(listPosition).getPickupaddress(), getActivity());
        LatLng endtLatLong = GetLatLongFromAddress.get(ordersInfoArrayList.get(listPosition).getDeliveryaddress(), getActivity());
//        List<LatLng> latLngList = new ArrayList<>();
//        latLngList.add(startLatLong);
//        latLngList.add(endtLatLong);
//
//        String encodedPath = PolyUtil.encode(latLngList);
//        fillData(encodedPath);

        MarkerOptions marker = new MarkerOptions().position(startLatLong).title("Pickup");
        MarkerOptions marker1 = new MarkerOptions().position(endtLatLong).title("Deliver");
        // adding marker
        mMap.addMarker(marker);
        mMap.addMarker(marker1);
        String url = getMapsApiDirectionsUrl(startLatLong, endtLatLong);
        ReadTask downloadTask = new ReadTask();
        // Start downloading json data from Google Directions API
        downloadTask.execute(url);

//        String url = GMapAPIDirection.getMapsApiDirectionsUrl(startLatLong, endtLatLong);
//
//        ReadTask downloadTask = new ReadTask(url, mMap, getActivity());
//        // Start downloading json data from Google Directions API
//        downloadTask.execute();
    }

    public void fillData(String encodedPath) {
//        if (route_info != null) {
//            distance_textView.setText(route_info.getfmtDistance());
//            time_textView.setText(route_info.getfmtTime());

        LatLng latLong = GetLatLongFromAddress.get(ordersInfoArrayList.get(listPosition).getDeliveryaddress(), getActivity());
        //draw map route
        if (mMap != null) {
            PolylineOptions rpathOptions = new PolylineOptions();
            rpathOptions.color(Color.parseColor("#600000ff"));
            rpathOptions.width(15.0f);
            rpathOptions.geodesic(false);
            rpathOptions.addAll(PolyUtil.decode(encodedPath));
            Polyline polyline = mMap.addPolyline(rpathOptions);


            MarkerOptions cpositionMarkerOptions = new MarkerOptions();
            cpositionMarkerOptions.draggable(false);
            cpositionMarkerOptions.position(new LatLng(latLong.latitude, latLong.longitude));
            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_action_pick_map);
            cpositionMarkerOptions.icon(icon);
            cpositionMarkerOptions.flat(true);

            Marker cpositionMarker = mMap.addMarker(cpositionMarkerOptions);

            CircleOptions circleMarkerOptions = new CircleOptions();
            circleMarkerOptions.center(new LatLng(latLong.latitude, latLong.longitude));
            circleMarkerOptions.radius(10);
            circleMarkerOptions.fillColor(Color.parseColor("#800000ff"));
            circleMarkerOptions.strokeWidth(1.0f);
            circleMarkerOptions.strokeColor(Color.parseColor("#0000ff"));
            Circle pcircleMarker = mMap.addCircle(circleMarkerOptions);

            LatLngBounds bounds = new LatLngBounds(latLong, latLong);

            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 16));

            mMap.animateCamera(CameraUpdateFactory.zoomOut());
        }

//        empty_frameLayout.setVisibility(View.GONE);
//    } else
//
//    {
//        empty_frameLayout.setVisibility(View.VISIBLE);
//    }
    }


    private String getMapsApiDirectionsUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;

    }

    private class ReadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String data = "";
            try {
                MapHttpConnection http = new MapHttpConnection();
                data = http.readUr(url[0]);


            } catch (Exception e) {
                // TODO: handle exception
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }

    }

    public class MapHttpConnection {
        public String readUr(String mapsApiDirectionsUrl) throws IOException {
            String data = "";
            InputStream istream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(mapsApiDirectionsUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                istream = urlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(istream));
                StringBuffer sb = new StringBuffer();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                data = sb.toString();
                br.close();


            } catch (Exception e) {
                Log.d("Exception while reading", e.toString());
            } finally {
                istream.close();
                urlConnection.disconnect();
            }
            return data;

        }
    }

    public class PathJSONParser {

        public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;
            try {
                jRoutes = jObject.getJSONArray("routes");
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();
                                hm.put("lat",
                                        Double.toString(((LatLng) list.get(l)).latitude));
                                hm.put("lng",
                                        Double.toString(((LatLng) list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;

        }

        private List<LatLng> decodePoly(String encoded) {
            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }
            return poly;
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            // TODO Auto-generated method stub
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(4);
                polyLineOptions.color(Color.BLUE);
            }

            mMap.addPolyline(polyLineOptions);

        }
    }
}
