package hnweb.com.acexpress.fragments.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.driver.HomeActivity;
import hnweb.com.acexpress.application.AppConstants;
import hnweb.com.acexpress.application.AppParams;
import hnweb.com.acexpress.application.AppUrls;
import hnweb.com.acexpress.application.MainApplication;
import hnweb.com.acexpress.models.OrdersInfo;
import hnweb.com.acexpress.utility.LoadingDialog;
import hnweb.com.acexpress.utility.ToastUlility;

/**
 * Created by neha on 7/6/2017.
 */

public class RequestDetailsFragment extends Fragment implements View.OnClickListener {

    Button acceptBTN, declineBTN;
    TextView myPaymentTV;
    SharedPreferences sharedPreferences;
    ArrayList<OrdersInfo> ordersInfoArrayList;
    int position;
    private LoadingDialog loadingDialog;
    String status;


    TextView customerNameTV, dateTV, timeTV, pAddressTV, dAddressTV, dimensionsTV, weightTV;
    ImageView imageView;


    public RequestDetailsFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_request_details, container, false);
        ((HomeActivity) getActivity()).titleLL.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).toolbar.setTitle("REQUEST DETAILS");
        loadingDialog = new LoadingDialog(getActivity());
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        ordersInfoArrayList = (ArrayList<OrdersInfo>) getArguments().getSerializable("LIST");
        position = getArguments().getInt("POSITION");
        status = getArguments().getString("STATUS");
//        setToolbarDrawer();
        init(myFragmentView);

        customerNameTV.setText(ordersInfoArrayList.get(position).getName());
        dateTV.setText(ordersInfoArrayList.get(position).getPickupdate());
        timeTV.setText(ordersInfoArrayList.get(position).getPicktime());
        pAddressTV.setText(ordersInfoArrayList.get(position).getPickupaddress());
        dAddressTV.setText(ordersInfoArrayList.get(position).getDeliveryaddress());
        dimensionsTV.setText(ordersInfoArrayList.get(position).getDimension());
        weightTV.setText(ordersInfoArrayList.get(position).getWeight());


        return myFragmentView;
    }

    public void init(View v) {

        customerNameTV = (TextView) v.findViewById(R.id.customerNameTV);
        imageView = (ImageView) v.findViewById(R.id.imageView);
        dateTV = (TextView) v.findViewById(R.id.dateTV);
        timeTV = (TextView) v.findViewById(R.id.timeTV);
        pAddressTV = (TextView) v.findViewById(R.id.pAddressTV);
        dAddressTV = (TextView) v.findViewById(R.id.dAddressTV);
        dimensionsTV = (TextView) v.findViewById(R.id.dimensionsTV);
        weightTV = (TextView) v.findViewById(R.id.weightTV);

        acceptBTN = (Button) v.findViewById(R.id.acceptBTN);
        declineBTN = (Button) v.findViewById(R.id.declineBTN);
        acceptBTN.setOnClickListener(this);
        declineBTN.setOnClickListener(this);

        if (AppConstants.NEWREQUEST.equalsIgnoreCase("NEW")) {
            acceptBTN.setVisibility(View.VISIBLE);
            declineBTN.setText("DECLINE");

        } else if (AppConstants.NEWREQUEST.equalsIgnoreCase("PENDING")) {
            acceptBTN.setVisibility(View.GONE);
            declineBTN.setText("VIEW MAP");
        } else if (AppConstants.NEWREQUEST.equalsIgnoreCase("ONGOING")) {
            acceptBTN.setVisibility(View.GONE);
            declineBTN.setText("VIEW MAP");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptBTN:
                doAcceptDecline(sharedPreferences.getInt("USER_ID", 0), "ACCEPTED");
                ToastUlility.show(getActivity(), "Request Accepted....Under Development.....");
                break;
            case R.id.declineBTN:
                if (AppConstants.NEWREQUEST.equalsIgnoreCase("NEW")) {
                    doAcceptDecline(sharedPreferences.getInt("USER_ID", 0), "DECLINED");
                    ToastUlility.show(getActivity(), "Request Declined....Under Development....");
                } else {
                    PickupDeliveryFragment pdf = new PickupDeliveryFragment();
                    ((HomeActivity) getActivity()).replaceFragmentWithParams(pdf, ordersInfoArrayList, position,status);

//                    Intent intent = new Intent(getActivity(), PickupDeliveryActivity.class);
//                    intent.putExtra("ESTIMATE_ID", ordersInfoArrayList.get(position).getEstimate_id());
//                    startActivity(intent);
//                    getActivity().finish();
                }

                break;
        }
    }

    public void doAcceptDecline(final int user_id, final String status) {
        loadingDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.ACCEPDECLINE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("REgister", response);
//                ToastUlility.show(All.this, response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int msg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (msg_code == 1) {

                        ToastUlility.show(getActivity(), message);
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        startActivity(intent);
                        getActivity().finish();


                    } else {
//                        String message = jobj.getString("message");
                        ToastUlility.show(getActivity(), message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                ToastUlility.show(getActivity(), "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(AppParams.ESTIMATE_ID, ordersInfoArrayList.get(position).getEstimate_id());
                params.put(AppParams.REQUEST_STATUS, status);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };


        // Add the request to the RequestQueue.
        String request_tag = "new_orders";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

}
