package hnweb.com.acexpress.models;

import com.android.volley.Request;

import java.util.Map;

/**
 * Created by neha on 2/20/2017.
 */

public class RequestInfo {
    public static final int METHOD_GET = Request.Method.GET;
    public static final int METHOD_POST = Request.Method.POST;
    private int method;
    private Map<String, String> params;
    private String url;
    private String request_tag;
    private int request_id;


    public static int getMethodGet() {
        return METHOD_GET;
    }

    public static int getMethodPost() {
        return METHOD_POST;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequest_tag() {
        return request_tag;
    }

    public void setRequest_tag(String request_tag) {
        this.request_tag = request_tag;
    }

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int request_id) {
        this.request_id = request_id;
    }


}
