package hnweb.com.acexpress.models;

import java.io.Serializable;

/**
 * Created by neha on 6/6/2017.
 */

public class ViewEstimates implements Serializable{

    String estimate_id, pickupdate, picktime, pickupaddress, deliveryaddress, weight, dimension, distance,
            duration, userid_customer, userid_driver, type_of_vehicle, delivery_time, request_status,
            mileage_cost, fuel_charges, total_charges;

    public String getEstimate_id() {
        return estimate_id;
    }

    public void setEstimate_id(String estimate_id) {
        this.estimate_id = estimate_id;
    }

    public String getPickupdate() {
        return pickupdate;
    }

    public void setPickupdate(String pickupdate) {
        this.pickupdate = pickupdate;
    }

    public String getPicktime() {
        return picktime;
    }

    public void setPicktime(String picktime) {
        this.picktime = picktime;
    }

    public String getPickupaddress() {
        return pickupaddress;
    }

    public void setPickupaddress(String pickupaddress) {
        this.pickupaddress = pickupaddress;
    }

    public String getDeliveryaddress() {
        return deliveryaddress;
    }

    public void setDeliveryaddress(String deliveryaddress) {
        this.deliveryaddress = deliveryaddress;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getUserid_customer() {
        return userid_customer;
    }

    public void setUserid_customer(String userid_customer) {
        this.userid_customer = userid_customer;
    }

    public String getUserid_driver() {
        return userid_driver;
    }

    public void setUserid_driver(String userid_driver) {
        this.userid_driver = userid_driver;
    }

    public String getType_of_vehicle() {
        return type_of_vehicle;
    }

    public void setType_of_vehicle(String type_of_vehicle) {
        this.type_of_vehicle = type_of_vehicle;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getMileage_cost() {
        return mileage_cost;
    }

    public void setMileage_cost(String mileage_cost) {
        this.mileage_cost = mileage_cost;
    }

    public String getFuel_charges() {
        return fuel_charges;
    }

    public void setFuel_charges(String fuel_charges) {
        this.fuel_charges = fuel_charges;
    }

    public String getTotal_charges() {
        return total_charges;
    }

    public void setTotal_charges(String total_charges) {
        this.total_charges = total_charges;
    }



}
