package hnweb.com.acexpress.utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import hnweb.com.acexpress.activity.MainActivity;

/**
 * Created by shree on 19-10-2016.
 */
public class AlertUtility {
    public static void showAlert(Context context, boolean is_success, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (is_success) {
            builder.setTitle("Success");
        } else {
            builder.setTitle("Error");
        }

        builder.setMessage(msg);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void logout(final Activity context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context,
                        MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
//                toastDialog(context, "You are logout successfully");
                Toast.makeText(context,
                        "You are logout successfully", Toast.LENGTH_LONG).show();
                SharedPreferences settings = context.getApplicationContext()
                        .getSharedPreferences(context.getPackageName(),
                                Context.MODE_PRIVATE);
                settings.edit().clear().commit();
                context.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();


    }

}
