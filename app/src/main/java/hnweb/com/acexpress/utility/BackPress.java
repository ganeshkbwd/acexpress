package hnweb.com.acexpress.utility;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

/**
 * Created by neha on 6/13/2017.
 */

public class BackPress {
    private static boolean doubleBackToExitPressedOnce = false;

    public static void onBack(Activity activity) {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            activity.finish();
            return;
        }
        doubleBackToExitPressedOnce = true;
        Toast.makeText(activity, "Please click back again to exit from Ace Xpress App", Toast.LENGTH_SHORT)
                .show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
