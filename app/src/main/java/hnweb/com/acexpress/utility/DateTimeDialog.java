package hnweb.com.acexpress.utility;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by neha on 2/6/2017.
 */

public class DateTimeDialog {
    public int mYear, mMonth, mDay, mHour, mMinute;
    public String date;
    String time;


    public void getDate(Context context, final EditText pickupDateET) {

// Get Current Date

//        final TimeZone timeZone = TimeZone.getTimeZone("UTC");
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Log.e("DATE", dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        pickupDateET.setText(date);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();

    }


    public void getTime(final Context context, final EditText pickupTimeET) {
        // Get Current Time
//        final TimeZone timeZone1 = TimeZone.getTimeZone("UTC");
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE)+5;

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

//                        Calendar c2 = Calendar.getInstance();
//
//                        if (mYear == c2.get(Calendar.YEAR)
//                                && mMonth == c2.get(Calendar.MONTH)
//                                && mDay == c2.get(Calendar.DAY_OF_MONTH)
//                                && (mHour < c2.get(Calendar.HOUR_OF_DAY) || (mHour == c2.get(Calendar.HOUR_OF_DAY) && mMinute <= (c2.get(Calendar.MINUTE)+10))
//                        )
//                                ) {
//                            Toast.makeText(context, "Set time at least 10 minutes from now", Toast.LENGTH_LONG).show();
//                        } else {
                            Log.e("Time ", hourOfDay + ":" + minute);
                            time = hourOfDay + ":" + minute;
//                        Log.e("Time", date + " " + Time);
                            pickupTimeET.setText(time);
//                        }



                    }
                }, mHour, mMinute, false);

        timePickerDialog.show();

    }

}
