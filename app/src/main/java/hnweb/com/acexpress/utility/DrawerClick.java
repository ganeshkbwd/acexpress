package hnweb.com.acexpress.utility;

import android.app.Activity;
import android.content.Intent;

import hnweb.com.acexpress.R;
import hnweb.com.acexpress.activity.EditProfileActivity;
import hnweb.com.acexpress.activity.MyProfileActivity;
import hnweb.com.acexpress.activity.customer.FindDriversActivity;
import hnweb.com.acexpress.activity.customer.MyOrdersActivity;
import hnweb.com.acexpress.activity.customer.NotificationCActivity;
import hnweb.com.acexpress.activity.driver.HomeActivity;
import hnweb.com.acexpress.activity.driver.MyPaymentActivity;
import hnweb.com.acexpress.application.AppConstants;

/**
 * Created by neha on 1/31/2017.
 */

public class DrawerClick {

    public static void drawerClick(Activity context, int id) {

        switch (id) {
            case R.id.homeBTN:
                if (AppConstants.ISDRIVER) {
                    intentCall(context, HomeActivity.class);
                } else {
                    intentCall(context, FindDriversActivity.class);
                }

                break;
            case R.id.myprofileBTN:
//                intentCall(context, MyProfileActivity.class);
                Intent intent = new Intent(context, MyProfileActivity.class);
                intent.putExtra("ComeFrom", "No");
                context.startActivity(intent);
                break;
            case R.id.myorderBTN:
                if (AppConstants.ISDRIVER) {
                    intentCall(context, MyPaymentActivity.class);
                } else {
                    intentCall(context, MyOrdersActivity.class);
                }

                break;
            case R.id.notificationBTN:
                if (AppConstants.ISDRIVER) {
                    intentCall(context, HomeActivity.class);
                } else {
                    intentCall(context, NotificationCActivity.class);
                }

//                intentCall(context, FindDriversActivity.class);
                break;
            case R.id.settingsBTN:
//                intentCall(context, FindDriversActivity.class);
                break;
            case R.id.logoutBTN:
                AlertUtility.logout(context);
//                intentCall(context, FindDriversActivity.class);
                break;
            case R.id.editBTN:
                intentCall(context, EditProfileActivity.class);
                break;
            case R.id.titleLL:
                break;
            case R.id.blankLL:
                break;
        }
    }

    public static void intentCall(Activity context, Class toClass) {
        Intent intent = new Intent(context, toClass);
        context.startActivity(intent);
        context.finish();
    }


}
