package hnweb.com.acexpress.utility;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import hnweb.com.acexpress.activity.customer.FindDriversActivity;

/**
 * Created by neha on 2/14/2017.
 */

public class GPS {

    public static void checkGPS(final Activity activity) {
        try {
            int off = Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.LOCATION_MODE);

            if (off == 0) {
                AlertDialog.Builder dialog_bulder = new AlertDialog.Builder(activity);
                dialog_bulder.setTitle("GPS Location");
                dialog_bulder.setMessage("Please Turn on GPS");
                dialog_bulder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                        Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        activity.startActivity(onGPS);
                    }
                });
                dialog_bulder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();

                    }
                });
                AlertDialog dialog = dialog_bulder.create();
                dialog.show();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }
}
