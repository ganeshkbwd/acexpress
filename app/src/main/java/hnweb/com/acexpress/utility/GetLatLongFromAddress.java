package hnweb.com.acexpress.utility;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by neha on 7/7/2017.
 */

public class GetLatLongFromAddress {
    public static LatLng get(String strAddress, Activity context) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng((double) location.getLatitude(),
                    (double) location.getLongitude());

            return p1;
        } catch (Exception e) {
            return null;
        }
    }
}
