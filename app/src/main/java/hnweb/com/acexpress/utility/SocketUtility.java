package hnweb.com.acexpress.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.AsyncSocket;
import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.callback.ConnectCallback;
import com.koushikdutta.async.callback.DataCallback;
import com.koushikdutta.async.callback.WritableCallback;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HNWEB PC-01 on 0019 19-09-2016.
 */
public class SocketUtility implements CompletedCallback, DataCallback, WritableCallback {
    private final Context context;
    private String tcpServerAddress;
    private int tcpServerPort;
    private OnConnectionListner connection_listner;
    private AsyncServer aSyncServer;
    private AsyncSocket socket;
    private ConnectivityManager cmanager;

    public void disconnect() {
        if (socket != null && socket.isOpen()) {
            socket.end();
            socket.close();
            if (connection_listner != null) {
                connection_listner.OnDisconnected();
            }
        }

    }

    public synchronized boolean sendCommand(JSONObject jobj) {
        if (socket != null && socket.isOpen()) {
            socket.setWriteableCallback(new WritableCallback() {
                @Override
                public void onWriteable() {

                }
            });
            StringBuilder sdata = new StringBuilder();
            sdata.append(jobj.toString());
            sdata.append("\r\n");
            socket.write(new ByteBufferList(sdata.toString().getBytes()));
            return true;
        }

        return false;
    }

    @Override
    public void onCompleted(Exception ex) {
        Log.i("SocketUtility", "onCompleted ");
    }

    @Override
    public void onDataAvailable(DataEmitter emitter, ByteBufferList bb) {
        String response = new String(bb.getAllByteArray());
        Log.i("SocketUtility", "onDataAvailable response= " + response);
        if (connection_listner != null) {
            try {
                JSONObject obj = new JSONObject(response);
                connection_listner.OnReceiveData(obj);
            } catch (JSONException e) {
                e.printStackTrace();
                JSONObject obj = new JSONObject();
                connection_listner.OnReceiveData(obj);
            }
        }
    }


    @Override
    public void onWriteable() {
        Log.i("SocketUtility", "onWriteable ");
    }

    public boolean isConnected() {
        if (socket != null && socket.isOpen()) {
            return true;
        }
        return false;
    }

    public void onStart() {
        connect();
        //register network listner

        context.registerReceiver(networkChangeListner, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    public void onStop() {
        disconnect();
        //unregister receiver
        context.unregisterReceiver(networkChangeListner);
    }

    public interface OnConnectionListner {
        public void OnConnected(AsyncSocket clientSocket);

        public void OnReceiveData(JSONObject obj);

        public void OnDisconnected();

        public void OnError();

    }

    public SocketUtility(Context context) {
        this.context = context;
    }

    public void setServerConfig(String tcpServerAddress, int tcpServerPort) {
        this.tcpServerAddress = tcpServerAddress;
        this.tcpServerPort = tcpServerPort;
        this.aSyncServer = AsyncServer.getDefault();
        this.cmanager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

    }

    public void connect() {
        if (aSyncServer != null) {
            if (aSyncServer.isRunning()) {
                aSyncServer.stop();
            }
            aSyncServer.connectSocket(tcpServerAddress, tcpServerPort, new ConnectCallback() {


                @Override
                public void onConnectCompleted(Exception ex, AsyncSocket sock) {
                    if (sock != null) {
                        socket = sock;
                        setListners();
                        if (connection_listner != null) {
                            connection_listner.OnConnected(socket);
                        }
                    } else {
                        if (connection_listner != null) {
                            connection_listner.OnError();
                        }
                    }
                }
            });
        }

    }

    private BroadcastReceiver networkChangeListner = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Log.i("networkChangeListner","onReceive");
            NetworkInfo activeNetwork = cmanager.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (isConnected) {
                connect();
            } else {
                disconnect();
            }
        }
    };

    private void setListners() {
        if (socket != null) {
            socket.setClosedCallback(this);
            socket.setDataCallback(this);
            socket.setEndCallback(this);
            socket.setWriteableCallback(this);
        }
    }

    public void setConnectionCallbackListner(OnConnectionListner listner) {
        this.connection_listner = listner;
    }

}
